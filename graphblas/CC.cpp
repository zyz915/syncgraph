// Boruvka's MSF algorithm simplified for finding connected components

#include "graphblas.h"
#include "io.h"

using namespace graphblas;

class CompareAndSwap : public BinaryOp<int> {
public:
	CompareAndSwap(int val):val_(val) {}
	int func(const int &a, const int &b) override {
		return (b != val_ ? b : a);
	}
private:
	int val_;
};

void Boruvka_cc (GraphBLAS &GrB, Matrix<bool> &A, Vector<int> &f)
{
	int n = A.getDim();
	f.setToIndex();
	Vector<int> p(GrB.getCommGrid(), n);
	Vector<int> m(GrB.getCommGrid(), n);
	Vector<int> i(GrB.getCommGrid(), n);
	Vector<int> e(GrB.getCommGrid(), n);
	Monoid<int> mMin(MIN_INT32, n);
	CompareAndSwap cas(n);
	i.setToIndex();
	int *fr = f.getRowPtr(), *fc = f.getColPtr();
	int rem = GrB.Matrix_nvals(A);
	while (rem > 0) {
		GrB.vxm<int>(m, mMin, f, A);
		GrB.assign(m, MIN_INT32, m, f);
		GrB.eWiseMult(e, ISEQ_INT32, f, i);
		GrB.assign(p, n); // p(:) := n
		GrB.assign(p, e, m); // p[i] := m[i] if e[i]
		GrB.eWiseMult(f, cas, f, p);
		// if (f[f[u]] == u) f[u] = min(f[u], f[f[u]])
		GrB.assign(p, INT_MAX); // p(:) = inf
		GrB.extract(p, MIN_INT32, f, f); // p[i] = f[f[i]]
		GrB.eWiseMult(e, ISEQ_INT32, p, i);
		GrB.eWiseMult(p, MIN_INT32, f, p);
		GrB.assign(f, e, p);
		do {
			GrB.assign(p, f); // p = f
			GrB.assign(f, INT_MAX);
			GrB.extract(f, MIN_INT32, p, p);
		} while (p != f);
		GrB.select(A, [fr, fc](int u, int v) {return (fr[u] != fc[v]);} );
		rem = GrB.Matrix_nvals(A);
	}
}

int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	CommGrid *commGrid = new CommGrid(MPI_COMM_WORLD);
	// load file
	Param param = parseParam(argc, argv);
	CSCFormat<bool> csc;
	loadGraph(commGrid, param, csc);
	GraphBLAS GrB(commGrid, csc.n);
	// compute
	double t1 = MPI_Wtime();
	Vector<int> f(commGrid, csc.n);
	Boruvka_cc(GrB, csc, f);
	double t2 = MPI_Wtime();
	// label CCs
	int nCC;
	Monoid<int> mAdd(PLUS_INT32, 0);
	Vector<int> g(commGrid, csc.n);
	g.setToIndex();
	GrB.eWiseMult(g, ISEQ_INT32, g, f);
	GrB.reduce(nCC, mAdd, g);

	int ncc = 0;
	if (commGrid->getRank() == 0) {
		printf("number of components: %d\n", nCC);
		printf("elapsed time: %f\n", t2 - t1);
	}
}
