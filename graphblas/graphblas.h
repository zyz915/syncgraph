#ifndef GRAPHBLAS_H_
#define GRAPHBLAS_H_

#include <functional>
#include <algorithm>
#include <cstdint>
#include <climits>

#include "global.h"
#include "fully_dist_vec.h"
#include "commgrid.h"

namespace graphblas {

template<typename T>
using Matrix = CSCFormat<T>;

template<typename T>
using Vector = FullyDistVec<T>;

template<typename T>
class UnaryOp {
public:
	virtual T func(const T &a) = 0;
	virtual std::function<T(T)> unaryOp() final {
		return [this](const T &a) { return func(a); };
	}
};

template<typename T>
class BinaryOp {
public:
	virtual T func(const T &a, const T &b) = 0;
	virtual std::function<T(T, T)> binaryOp() final {
		return [this](const T &a, const T &b) { return func(a, b); };
	}
};

template<typename T>
class Accumulator : public BinaryOp<T> {
public:
	virtual MPI_Op mpiOp() = 0;
};

template<typename T>
class Monoid : public Accumulator<T> {
public:
	Monoid(Accumulator<T> &accum, const T &id):
			accum_(accum), id_(id) {}

	T func(const T &a, const T &b) override {
		return accum_.func(a, b);
	}

	MPI_Op mpiOp() override {
		return accum_.mpiOp();
	}

	const T& id() const {
		return id_;
	}

private:
	Accumulator<T> &accum_;
	T id_;
};

class GraphBLAS {
public:
	GraphBLAS(CommGrid *cg, int n);

	template<typename T>
	void vxm(Vector<T> &y, Accumulator<T> &accum, Vector<T> &x, const Matrix<bool> &A);
	template<typename T>
	void vxm(Vector<T> &y, Accumulator<T> &accum, Monoid<T> &monoid, Vector<T> &x, const Matrix<bool> &A);
	template<typename T>
	void vxm(Vector<T> &y, Monoid<T> &monoid, Vector<T> &x, const Matrix<bool> &A);

	template<typename T>
	void assign(Vector<T> &y, Accumulator<T> &accum, Vector<T> &x, Vector<T> &index);
	template<typename T>
	void assign(Vector<T> &y, const T &val);
	template<typename T, typename T2>
	void assign(Vector<T> &y, Vector<T2> &m, Vector<T> &x);
	template<typename T>
	void assign(Vector<T> &y, Vector<T> &x);

	template<typename T>
	void apply(Vector<T> &y, BinaryOp<T> &bop, UnaryOp<T> &uop, Vector<T> &x);
	template<typename T>
	void apply(Vector<T> &y, UnaryOp<T> &op, Vector<T> &x);

	template<typename T>
	void extract(Vector<T> &y, Accumulator<T> &accum, Vector<T> &x, Vector<int> &index);
	template<typename T>
	void extract(Vector<T> &y, Vector<T> &x, Vector<int> &index);

	template<typename T>
	void eWiseMult(Vector<T> &z, BinaryOp<T> &op, Vector<T> &x, Vector<T> &y);

	template<typename T>
	void reduce(T &out, Monoid<T> monoid, Vector<T> x);

	void select(Matrix<bool> &csc, std::function<bool(int, int)> pred);
	template<typename T>
	void select(Matrix<T> &csc, std::function<bool(int, int, T)> pred);

	template<typename T>
	int64_t Matrix_nvals(const Matrix<T> &A);

	CommGrid *getCommGrid();

private:
	CommGrid *commGrid;
	int rStart, rEnd, rLength;  // range of row vector
	int cStart, cEnd, cLength;  // range of col vector
};

GraphBLAS::GraphBLAS(CommGrid *cg, int n):commGrid(cg) {
	getRange(n, commGrid->getRowDim(), commGrid->getRowGroup(), rStart, rEnd);
	getRange(n, commGrid->getColDim(), commGrid->getColGroup(), cStart, cEnd);
	rLength = rEnd - rStart;
	cLength = cEnd - cStart;
}

template<typename T>
void GraphBLAS::vxm(Vector<T> &y, Accumulator<T> &accum, Vector<T> &x, const Matrix<bool> &A) {
	T *x_row = x.getRowPtr();
	T *y_col = y.getColPtr();
	#pragma omp parallel for
	for (int v = 0; v < cLength; v++)
		for (int64_t i = A.pos[v]; i < A.pos[v + 1]; i++) {
			int u = A.data[i];
			y_col[v] = accum.func(y_col[v], x_row[u]);
		}
	y.syncColToRow(accum.mpiOp());
}

template<typename T>
void GraphBLAS::vxm(Vector<T> &y, Accumulator<T> &accum, Monoid<T> &monoid, Vector<T> &x, const Matrix<bool> &A) {
	Vector<T> w(x.getCommGrid(), A.n);
	w.col().fill(monoid.id()); // identity
	T *x_row = x.getRowPtr();
	T *w_col = w.getColPtr();
	#pragma omp parallel for
	for (int v = 0; v < cLength; v++)
		for (int64_t i = A.pos[v]; i < A.pos[v + 1]; i++) {
			int u = A.data[i];
			w_col[v] = monoid.func(w_col[v], x_row[u]);
		}
	w.syncColToRow(monoid.mpiOp());
	y.zip(w, accum.binaryOp());
}

template<typename T>
void GraphBLAS::vxm(Vector<T> &y, Monoid<T> &monoid, Vector<T> &x, const Matrix<bool> &A) {
	y.col().fill(monoid.id()); // identity
	T *x_row = x.getRowPtr();
	T *y_col = y.getColPtr();
	#pragma omp parallel for
	for (int v = 0; v < cLength; v++)
		for (int64_t i = A.pos[v]; i < A.pos[v + 1]; i++) {
			int u = A.data[i];
			y_col[v] = monoid.func(y_col[v], x_row[u]);
		}
	y.syncColToRow(monoid.mpiOp());
}

template<typename T>
void GraphBLAS::assign(Vector<T> &y, Accumulator<T> &accum, Vector<T> &x, Vector<T> &index) {
	int *i_col = index.getColPtr();
	T *x_col = x.getColPtr();
	T *y_row = y.getRowPtr();
	for (int v = 0; v < cLength; v++)
		if (i_col[v] >= rStart && i_col[v] < rEnd) {
			int u = i_col[v] - rStart;
			y_row[u] = accum.func(y_row[u], x_col[v]);
		}
	y.syncRowToCol(accum.mpiOp());
}

template<typename T>
void GraphBLAS::assign(Vector<T> &y, const T &val) {
	y.fill(val);
}

template<typename T, typename T2>
void GraphBLAS::assign(Vector<T> &y, Vector<T2> &m, Vector<T> &x) {
	int *m_row = m.getRowPtr(), *m_col = m.getColPtr();
	int *x_row = x.getRowPtr(), *x_col = x.getColPtr();
	int *y_row = y.getRowPtr(), *y_col = y.getColPtr();
	for (int i = 0; i < rLength; i++)
		if (m_row[i])
			y_row[i] = x_row[i];
	for (int i = 0; i < cLength; i++)
		if (m_col[i])
			y_col[i] = x_col[i];
}

template<typename T>
void GraphBLAS::assign(Vector<T> &y, Vector<T> &x) { // copy
	y = x;
}

template<typename T>
void GraphBLAS::apply(Vector<T> &y, BinaryOp<T> &bop, UnaryOp<T> &uop, Vector<T> &x) {
	auto f = [&bop, &uop](const T &y, const T &x) { return bop.func(x, uop.func(y)); };
	y.zip(y, x, f);
}

template<typename T>
void GraphBLAS::apply(Vector<T> &y, UnaryOp<T> &op, Vector<T> &x) {
	y.map(x, op.unaryOp());
}

template<typename T>
void GraphBLAS::extract(Vector<T> &y, Accumulator<T> &accum, Vector<T> &x, Vector<int> &index) {
	int *i_row = index.getRowPtr();
	T *x_row = x.getRowPtr(), *x_col = x.getColPtr();
	T *y_row = y.getRowPtr();
	for (int u = 0; u < rLength; u++)
		if (i_row[u] >= cStart && i_row[u] < cEnd) {
			int v = i_row[u] - cStart;
			y_row[u] = accum.func(y_row[u], x_col[v]);
		}
	y.syncRowToCol(accum.mpiOp());
}

template<typename T>
void GraphBLAS::extract(Vector<T> &y, Vector<T> &x, Vector<int> &index) {
	int *i_row = index.getRowPtr();
	T *x_row = x.getRowPtr();
	T *y_row = y.getRowPtr();
	y.row().fill(std::numeric_limits<T>::max());
	for (int u = 0; u < rLength; u++)
		if (i_row[u] >= cStart && i_row[u] < cEnd) {
			int v = i_row[u] - cStart;
			y_row[u] = x_row[v];
		}
	y.syncRowToCol(MPI_MIN);
}

template<typename T>
void GraphBLAS::eWiseMult(Vector<T> &z, BinaryOp<T> &op, Vector<T> &x, Vector<T> &y) {
	z.zip(x, y, op.binaryOp());
}

template<typename T>
void GraphBLAS::reduce(T &out, Monoid<T> monoid, Vector<T> x) {
	out = x.reduce(monoid.binaryOp(), monoid.id(), monoid.mpiOp());
}

void GraphBLAS::select(Matrix<bool> &csc, std::function<bool(int, int)> pred) {
	int nthreads = get_num_threads();
	int64_t *range = new int64_t[nthreads + 1];
	int64_t *count = new int64_t[nthreads + 1];
	range[0] = 0;
	for (int i = 0; i < nthreads; i++)
		range[i + 1] = range[i] + (cLength + i) / nthreads;

	#pragma omp parallel for num_threads(nthreads)
	for (int id = 0; id < nthreads; id++) {
		int64_t ptr = csc.pos[range[id]];
		for (int v = range[id]; v < range[id + 1]; v++) {
			int64_t start = csc.pos[v];
			csc.pos[v] = ptr;
			for (int64_t i = start; i < csc.pos[v + 1]; i++) {
				int u = csc.data[i];
				if (pred(u, v))
					csc.data[ptr++] = u;
			}
		}
		count[id] = ptr - csc.pos[range[id]];
	}

	int64_t offset = 0;
	for (int i = 0; i < nthreads; i++) {
		int *ptr = csc.data + csc.pos[range[i]];
		std::copy(ptr, ptr + count[i], csc.data + offset);
		offset += count[i];
		count[i] = offset - count[i];
	}

	#pragma omp parallel for num_threads(nthreads)
	for (int id = 0; id < nthreads; id++) {
		int64_t ptr = csc.pos[range[id]];
		for (int v = range[id]; v < range[id + 1]; v++)
			csc.pos[v] -= ptr - count[id];
	}
	csc.pos[cLength] = offset;
	delete[] count;
	delete[] range;
}

template<typename T>
void GraphBLAS::select(Matrix<T> &csc, std::function<bool(int, int, T)> pred) {
	int nthreads = get_num_threads();
	int64_t ptr = 0, start = 0;
	int64_t *range = new int64_t[nthreads + 1];
	int64_t *count = new int64_t[nthreads + 1];
	range[0] = 0;
	for (int i = 0; i < nthreads; i++)
		range[i + 1] = range[i] + (cLength + i) / nthreads;

	#pragma omp parallel for num_threads(nthreads)
	for (int id = 0; id < nthreads; id++) {
		int64_t ptr = csc.pos[range[id]];
		for (int v = range[id]; v < range[id + 1]; v++) {
			int64_t start = csc.pos[v];
			csc.pos[v] = ptr;
			for (int64_t i = start; i < csc.pos[v + 1]; i++) {
				int u = csc.data[i];
				T w = csc.weight[i];
				if (pred(u, v, w)) {
					csc.data[ptr] = u;
					csc.weight[ptr] = w;
					ptr++;
				}
			}
		}
		count[id] = ptr - csc.pos[range[id]];
	}

	int64_t offset = 0;
	for (int i = 0; i < nthreads; i++) {
		int *ptr_d = csc.data + csc.pos[range[i]];
		int *ptr_w = csc.weight + csc.pos[range[i]];
		std::copy(ptr_d, ptr_d + count[i], csc.data + offset);
		std::copy(ptr_w, ptr_w + count[i], csc.weight + offset);
		offset += count[i];
		count[i] = offset - count[i];
	}

	#pragma omp parallel for num_threads(nthreads)
	for (int id = 0; id < nthreads; id++) {
		int64_t ptr = csc.pos[range[id]];
		for (int v = range[id]; v < range[id + 1]; v++)
			csc.pos[v] -= ptr - count[id];
	}
	csc.pos[cLength] = offset;
	delete[] count;
	delete[] range;
}

template<typename T>
int64_t GraphBLAS::Matrix_nvals(const Matrix<T> &A) {
	return allReduce(A.pos[cLength], MPI_SUM, commGrid->getCommWorld());
}

CommGrid* GraphBLAS::getCommGrid() {
	return commGrid;
}

template<typename T>
class Min : public Accumulator<T> {
public:
	T func(const T &a, const T &b) override {
		return std::min(a, b);
	}

	MPI_Op mpiOp() override {
		return MPI_MIN;
	}
};

class MinI8  : public Min<int8_t> {} MIN_INT8;
class MinI16 : public Min<int16_t> {} MIN_INT16;
class MinI32 : public Min<int32_t> {} MIN_INT32;
class MinI64 : public Min<int64_t> {} MIN_INT64;
class MinU8  : public Min<uint8_t> {} MIN_UINT8;
class MinU16 : public Min<uint16_t> {} MIN_UINT16;
class MinU32 : public Min<uint32_t> {} MIN_UINT32;
class MinU64 : public Min<uint64_t> {} MIN_UINT64;
class MinF32 : public Min<float> {} MIN_FP32;
class MinF64 : public Min<double> {} MIN_FP64;

template<typename T>
class Max : public Accumulator<T> {
public:
	T func(const T &a, const T &b) override {
		return std::min(a, b);
	}

	MPI_Op mpiOp() override {
		return MPI_MAX;
	}
};

class MaxI8  : public Max<int8_t> {} MAX_INT8;
class MaxI16 : public Max<int16_t> {} MAX_INT16;
class MaxI32 : public Max<int32_t> {} MAX_INT32;
class MaxI64 : public Max<int64_t> {} MAX_INT64;
class MaxU8  : public Max<uint8_t> {} MAX_UINT8;
class MaxU16 : public Max<uint16_t> {} MAX_UINT16;
class MaxU32 : public Max<uint32_t> {} MAX_UINT32;
class MaxU64 : public Max<uint64_t> {} MAX_UINT64;
class MaxF32 : public Max<float> {} MAX_FP32;
class MaxF64 : public Max<double> {} MAX_FP64;

template<typename T>
class PLUS : public Accumulator<T> {
public:
	T func(const T &a, const T &b) override {
		return a + b;
	}

	MPI_Op mpiOp() override {
		return MPI_SUM;
	}
};

class PLUSI8  : public PLUS<int8_t> {} PLUS_INT8;
class PLUSI16 : public PLUS<int16_t> {} PLUS_INT16;
class PLUSI32 : public PLUS<int32_t> {} PLUS_INT32;
class PLUSI64 : public PLUS<int64_t> {} PLUS_INT64;
class PLUSU8  : public PLUS<uint8_t> {} PLUS_UINT8;
class PLUSU16 : public PLUS<uint16_t> {} PLUS_UINT16;
class PLUSU32 : public PLUS<uint32_t> {} PLUS_UINT32;
class PLUSU64 : public PLUS<uint64_t> {} PLUS_UINT64;
class PLUSF32 : public PLUS<float> {} PLUS_FP32;
class PLUSF64 : public PLUS<double> {} PLUS_FP64;

template<typename T>
class Mul : public Accumulator<T> {
public:
	T func(const T &a, const T &b) override {
		return a * b;
	}

	MPI_Op mpiOp() override {
		return MPI_PROD;
	}
};

class MulI8  : public Mul<int8_t> {} MUL_INT8;
class MulI16 : public Mul<int16_t> {} MUL_INT16;
class MulI32 : public Mul<int32_t> {} MUL_INT32;
class MulI64 : public Mul<int64_t> {} MUL_INT64;
class MulU8  : public Mul<uint8_t> {} MUL_UINT8;
class MulU16 : public Mul<uint16_t> {} MUL_UINT16;
class MulU32 : public Mul<uint32_t> {} MUL_UINT32;
class MulU64 : public Mul<uint64_t> {} MUL_UINT64;
class MulF32 : public Mul<float> {} MUL_FP32;
class MulF64 : public Mul<double> {} MUL_FP64;

template<typename T>
class ISEQ : public BinaryOp<T> {
public:
	T func(const T &a, const T &b) override {
		return a == b;
	}
};

class ISEQI8  : public ISEQ<int8_t> {} ISEQ_INT8;
class ISEQI16 : public ISEQ<int16_t> {} ISEQ_INT16;
class ISEQI32 : public ISEQ<int32_t> {} ISEQ_INT32;
class ISEQI64 : public ISEQ<int64_t> {} ISEQ_INT64;
class ISEQU8  : public ISEQ<uint8_t> {} ISEQ_UINT8;
class ISEQU16 : public ISEQ<uint16_t> {} ISEQ_UINT16;
class ISEQU32 : public ISEQ<uint32_t> {} ISEQ_UINT32;
class ISEQU64 : public ISEQ<uint64_t> {} ISEQ_UINT64;
class ISEQF32 : public ISEQ<float> {} ISEQ_FP32;
class ISEQF64 : public ISEQ<double> {} ISEQ_FP64;

template<typename T>
class ISNE : public BinaryOp<T> {
public:
	T func(const T &a, const T &b) override {
		return a != b;
	}
};

class ISNEI8  : public ISNE<int8_t> {} ISNE_INT8;
class ISNEI16 : public ISNE<int16_t> {} ISNE_INT16;
class ISNEI32 : public ISNE<int32_t> {} ISNE_INT32;
class ISNEI64 : public ISNE<int64_t> {} ISNE_INT64;
class ISNEU8  : public ISNE<uint8_t> {} ISNE_UINT8;
class ISNEU16 : public ISNE<uint16_t> {} ISNE_UINT16;
class ISNEU32 : public ISNE<uint32_t> {} ISNE_UINT32;
class ISNEU64 : public ISNE<uint64_t> {} ISNE_UINT64;
class ISNEF32 : public ISNE<float> {} ISNE_FP32;
class ISNEF64 : public ISNE<double> {} ISNE_FP64;

} /* namespace graphblas */

#endif /* GRAPHBLAS_H_ */