// implement the FastSV algorithm for finding connected components

#include "graphblas.h"
#include "io.h"

using namespace graphblas;

void FastSV(GraphBLAS &GrB, const Matrix<bool> &A, Vector<int> &f)
{
	int n = A.getDim();
	f.setToIndex();
	Vector<int> gp(f), mngp(f), dup(f), mod(GrB.getCommGrid(), n);
	Monoid<int> mMin(MIN_INT32, n);
	Monoid<int> mAdd(PLUS_INT32, 0);
	int diff = n;
	while (diff > 0) {
		GrB.vxm<int>(mngp, MIN_INT32, gp, A);
		GrB.assign(f, MIN_INT32, mngp, f);
		GrB.eWiseMult(f, MIN_INT32, f, mngp);
		GrB.eWiseMult(f, MIN_INT32, f, gp);
		GrB.extract(gp, MIN_INT32, f, f);
		GrB.eWiseMult(mod, ISNE_INT32, dup, gp);
		GrB.reduce(diff, mAdd, mod);
		GrB.assign(dup, gp);
	}
}

int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	CommGrid *commGrid = new CommGrid(MPI_COMM_WORLD);
	// load file
	Param param = parseParam(argc, argv);
	CSCFormat<bool> csc;
	loadGraph(commGrid, param, csc);
	GraphBLAS GrB(commGrid, csc.n);
	// compute
	double t1 = MPI_Wtime();
	Vector<int> f(commGrid, csc.n);
	FastSV(GrB, csc, f);
	double t2 = MPI_Wtime();
	// label CCs
	int nCC;
	Monoid<int> mAdd(PLUS_INT32, 0);
	Vector<int> g(commGrid, csc.n);
	g.setToIndex();
	GrB.eWiseMult(g, ISEQ_INT32, g, f);
	GrB.reduce(nCC, mAdd, g);

	if (commGrid->getRank() == 0) {
		printf("number of components: %d\n", nCC);
		printf("elapsed time: %f\n", t2 - t1);
	}
}
