#ifndef CSR_FORMAT_H_
#define CSR_FORMAT_H_

#include <mpi.h>
#include <vector>
#include <cassert>
#include <cstdio>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <sys/stat.h>

#include "commgrid.h"
#include "graph.h"

using namespace std;

template<typename I, typename T>
struct CSRFormat {
	I        n;      // number of vertices
	int64_t  nnz;    // non-zero elements
	int64_t *pos;    // offset
	int     *colidx; // vertex value
	T       *weight;

	CSRFormat():pos(nullptr), colidx(nullptr), weight(nullptr) {}
	~CSRFormat() {}

	I getDim() const {
		return n;
	}

	void clear() {
		if (pos) delete[] pos;
		if (colidx) delete[] colidx;
		if (weight) delete[] weight;
		pos = nullptr;
		colidx = nullptr;
		weight = nullptr;
	}
};

template<typename I, typename T>
static void tuple2CSRFormat(const Range<I> &R, vector<EdgeUnit<I, T> > &elist,
		CSRFormat<I, T> &csr)
{
	csr.n = R.n;
	csr.nnz = elist.size();
	csr.pos = new int64_t[R.rLength + 1];
	csr.colidx = new int[csr.nnz];
	csr.weight = new   T[csr.nnz];
	if (elist.empty()) {
		memset(csr.pos, 0, sizeof(int64_t) * (R.rLength + 1));
		return;
	}
	csr.pos[0] = 0;
	I cur = 0;
	sort(elist.begin(), elist.end());
	for (int i = 0; i < R.rLength; i++) {
		I x = (I) i + R.rStart;
		while (cur < elist.size() && elist[cur].src == x) {
			csr.colidx[cur] = elist[cur].dst - R.cStart;
			csr.weight[cur] = elist[cur].weight;
			cur += 1;
		}
		csr.pos[i + 1] = cur;
	}
	assert(cur == elist.size());
}

// matrix market exchange format
// ignore the matrix values
template<typename I, typename T>
void loadMatrix(CommGrid *commGrid, Param &param, CSRFormat<I, T> &csr, bool weighted)
{
	FILE *f = fopen(param.fname, "r");
	if (f == NULL) {
		sprintf(buff, "error! cannot open %s", param.fname);
		reportError(commGrid->getRank(), buff);
	}

	while (fgets(buff, BUFF_LEN, f))
		if (*buff != '%') break;

	I r, c;
	int64_t nnz;
	stringstream ss(buff);
	ss >> r >> c >> nnz;
	if (r != c) {
		sprintf(buff, "%s is not a square matrix!", param.fname);
		reportError(commGrid->getRank(), buff);
	}
	if (commGrid->getRank() == 0)
		cout << "|V| " << r << " |E| " <<  nnz << endl;

	I n = param.n = r;
	int dim = commGrid->getRowDim();
	int rank = commGrid->getRank();
	int nprocs = commGrid->getSize();
	Range<I> R(commGrid, n);
	vector<EdgeUnit<I, T> > send, recv;

	while (fgets(buff, BUFF_LEN, f)) {
		I x = 0, y = 0;
		char *p = buff;
		do {
			x = (x << 3) + (x << 1) + ((*p++) - '0');
		} while (isdigit(*p));
		p++;
		do {
			y = (y << 3) + (y << 1) + ((*p++) - '0');
		} while (isdigit(*p));
		x--; y--;
		T z = (weighted ? parse<T>(p) : 0);
		if (x >= R.rStart && x < R.rEnd && y >= R.cStart && y < R.cEnd) {
			recv.push_back(EdgeUnit<I, T>(x, y, z));
			if (param.symm)
				send.push_back(EdgeUnit<I, T>(y, x, z));
		}
	}
	fclose(f);

	if (param.symm)
		exchange(commGrid, send, recv);
	tuple2CSRFormat(R, recv, csr);
}

// binary format storing a sequence of edge tuples
//   x1, y1, x2, y2, x3, y3 .. xn, yn
// where each number is a 4-byte unsigned int
// all the values are 0-based.
template<typename I, typename T>
void loadBinary(CommGrid *commGrid, Param &param, CSRFormat<I, T> &csr, bool weighted)
{
	const int CHUNKSIZE = 1 << 24;
	I n = param.n;
	int nprocs = commGrid->getSize();
	Range<I> R(commGrid, n);
	vector<EdgeUnit<I, T> > send, recv;
	int dim = commGrid->getRowDim();
	int rank = commGrid->getRank();

	FILE *f = fopen(param.fname, "rb");
	if (f == NULL) {
		sprintf(buff, "error! cannot open %s\n", param.fname);
		reportError(rank, buff);
	}
	struct stat st;
	long fsize = 0;
	if (stat(param.fname, &st) == 0)
		fsize = st.st_size;
	// read the graph
	int unitSize = sizeof(I) * 2;
	long end = fsize / unitSize, ptr = 0;
	I *es = new I[CHUNKSIZE * 2];
	while (ptr < end) {
		int cnt = min(CHUNKSIZE, (int)(end - ptr));
		int actual = fread(es, unitSize, cnt, f);
		for (int i = 0; i < cnt; i++) {
			I x = es[i * 2];
			I y = es[i * 2 + 1];
			T z = 0;
			if (x >= R.rStart && x < R.rEnd && y >= R.cStart && y < R.cEnd) {
				recv.push_back(EdgeUnit<I, T>(x, y, z));
				if (param.symm)
					send.push_back(EdgeUnit<I, T>(y, x, z));
			}
		}
		ptr += cnt;
	}
	delete[] es;
	fclose(f);

	if (param.symm)
		exchange(commGrid, send, recv);
	tuple2CSRFormat(R, recv, csr);
}

template<typename I, typename T>
void loadGraph(CommGrid *commGrid, Param &param, CSRFormat<I, T> &csr, bool weighted = false)
{
	if (commGrid->getRank() == 0)
		printf("filename: %s\n", param.fname);
	double t1 = MPI_Wtime();
	switch (param.type) {
		case 0: {
			loadMatrix(commGrid, param, csr, weighted);
			break;
		}
		case 1: {
			loadBinary(commGrid, param, csr, weighted);
			break;
		}
	}
	MPI_Barrier(commGrid->getCommWorld());
	double t2 = MPI_Wtime();
	if (commGrid->getRank() == 0)
		printf("finish reading: %fs\n", t2 - t1);
}

template<typename I, typename T>
void loadWeightedGraph(CommGrid *commGrid, Param &param, CSRFormat<I, T> &csr)
{
	return loadGraph(commGrid, param, csr, true);
}

#endif /* CSR_FORMAT_H_ */
