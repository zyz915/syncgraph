#ifndef CONSTANT_VEC_H_
#define CONSTANT_VEC_H_

#include "commgrid.h"
#include "dist_vec.h"

/**
 * Space-efficient implementation of several special vectors
 *
 * ConstantRowVec, ConstantColVec:
 *   - distributed vectors consisting of a constant value
 *
 * IndexRowVec, IndexColVec:
 *   - distributed vectors that constantly return the index (vec[i] == i)
 */

template<typename T>
class ConstantRowVec : public ReadableRowVec<T> {
public:
	ConstantRowVec(const T &v):val_(v) {}

	T operator[](size_t i) const override;
	const T& getValue() const;

private:
	T val_;
};

template<typename T>
class ConstantColVec : public ReadableColVec<T> {
public:
	ConstantColVec(const T &v):val_(v) {}

	T operator[](size_t i) const override;
	const T& getValue() const;

private:
	T val_;
};

template<typename T>
class ConstantVec : public ReadableVec<T> {
public:
	ConstantVec(const T &v):row_(v), col_(v) {}

	const ReadableRowVec<T>& row() const override { return row_; }
	const ReadableColVec<T>& col() const override { return col_; }

private:
	ConstantRowVec<T> row_;
	ConstantColVec<T> col_;
};

template<typename I>
class IndexRowVec : public ReadableRowVec<I> {
public:
	IndexRowVec(const Range<I> &R);

	I operator[](size_t i) const override;

private:
	I start_;
};

template<typename I>
class IndexColVec : public ReadableColVec<I> {
public:
	IndexColVec(const Range<I> &R);

	I operator[](size_t i) const override;

private:
	I start_;
};

template<typename I>
class IndexVec : public ReadableVec<I> {
public:
	IndexVec(const Range<I> &R):row_(R), col_(R) {}

	const ReadableRowVec<I>& row() const override { return row_; }
	const ReadableColVec<I>& col() const override { return col_; }

private:
	IndexRowVec<I> row_;
	IndexColVec<I> col_;
};

//--------------- ConstantRowVec ---------------

template<typename T>
T ConstantRowVec<T>::operator[](size_t i) const {
	return val_;
}

template<typename T>
const T& ConstantRowVec<T>::getValue() const {
	return val_;
}

//--------------- ConstantColVec ---------------

template<typename T>
T ConstantColVec<T>::operator[](size_t i) const {
	return val_;
}

template<typename T>
const T& ConstantColVec<T>::getValue() const {
	return val_;
}

//--------------- IndexRowVec ---------------

template<typename I>
IndexRowVec<I>::IndexRowVec(const Range<I> &R) {
	start_ = R.rStart;
}

template<typename I>
I IndexRowVec<I>::operator[](size_t i) const {
	return start_ + i;
}

//--------------- IndexColVec ---------------

template<typename I>
IndexColVec<I>::IndexColVec(const Range<I> &R) {
	start_ = R.cStart;
}

template<typename I>
I IndexColVec<I>::operator[](size_t i) const {
	return start_ + i;
}

#endif /* CONSTANT_VEC_H_ */
