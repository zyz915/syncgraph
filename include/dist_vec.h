#ifndef DIST_VEC_H_
#define DIST_VEC_H_

#include <algorithm>
#include <cstring>
#include <functional>
#include <iostream>
#include <memory>
#include <utility>
#include <vector>
#include <mpi.h>

#include "commgrid.h"
#include "global.h"
#include "mpitype.h"

/** 
 * RowVec, ColVec (Readable/Writable version):
 *  - distributed dense vector with replication
 *      > each element is replicated in its row/column group
 *  - Readable: the vector is used in the read-only mode
 *  - Writable: the vector can be modified by each process individually, and
 *              all the processes can synchronize the contents by MPI_Allreduce
 *
 * RowOwner, ColOwner:
 *  - distributed dense vector without replication (more space-efficient)
 *      > elements are owned by the current process
 *      > fast for element-wise operations but slow for other operations
 *  - by default readable and writable; no need to synchronize
 *
 * SpRowVec, SpColVec:
 *  - distributed sparse row/column with replication
 *      > each element is a tuple (index, value)
 *      > each element is replicated in its row/column group
 *  - the vector can be only used in the read-only mode
 *  - 
 */

template<typename T>
class ReadableRowVec {
public:
	ReadableRowVec() = default;
	virtual ~ReadableRowVec() {}

	virtual T operator[](size_t i) const = 0;
};

template<typename T>
class ReadableColVec {
public:
	ReadableColVec() = default;
	virtual ~ReadableColVec() {}

	virtual T operator[](size_t i) const = 0;
};

template<typename T>
class WritableRowVec {
public:
	WritableRowVec() = default;
	virtual ~WritableRowVec() {}

	virtual T& operator[](size_t i) = 0;
	virtual void set(const ReadableRowVec<T> &it) = 0;
	virtual void sync(MPI_Op op) = 0;
};

template<typename T>
class WritableColVec {
public:
	WritableColVec() = default;
	virtual ~WritableColVec() {}

	virtual T& operator[](size_t i) = 0;
	virtual void set(const ReadableColVec<T> &it) = 0;
	virtual void sync(MPI_Op op) = 0;
};

template<typename T>
class ReadableVec {
public:
	ReadableVec() = default;
	virtual ~ReadableVec() {}

	virtual const ReadableRowVec<T>& row() const = 0;
	virtual const ReadableColVec<T>& col() const = 0;
};

template<typename T>
class RowOwner {
public:
	RowOwner() {}
	virtual ~RowOwner() {}

	// by default readable and writable
	virtual T* r_data() = 0;
	virtual const T* r_data() const = 0;
	// size of owned data
	virtual int r_size() const = 0;

	template<typename I>
	void debugPrint(const Range<I> &R) const;
};

template<typename T>
class ColOwner {
public:
	ColOwner() {}
	virtual ~ColOwner() {}

	// by default readable and writable
	virtual T* c_data() = 0;
	virtual const T* c_data() const = 0;
	// size of owned data
	virtual int c_size() const = 0;

	template<typename I>
	void debugPrint(const Range<I> &R) const;
};

template<typename V>
template<typename I>
void RowOwner<V>::debugPrint(const Range<I> &R) const {
	auto commGrid = R.getCommGrid();
	int size = commGrid->getSize();
	int rDim = commGrid->getRowDim();
	int cDim = commGrid->getColDim();
	int *sendcnt = new int[size];
	int *sdispls = new int[size + 1];
	int length = r_size();
	MPI_Allgather(&length, 1, MPI_INT, sendcnt, 1, MPI_INT,
			commGrid->getCommWorld());
	sdispls[0] = 0;
	for (int i = 0; i < size; i++)
		sdispls[i + 1] = sdispls[i] + sendcnt[i];
	int total = sdispls[size];
	V *vals = new V[total];
	MPI_Datatype type = MPIType<V>();
	MPI_Gatherv(r_data(), length, type, vals, sendcnt, sdispls,
			type, 0, commGrid->getCommWorld());
	if (commGrid->getRank() == 0) {
		for (int i = 0; i < rDim; i++)
			for (int j = 0; j < cDim; j++) {
				int p = j * cDim + i;
				for (int k = sdispls[p]; k < sdispls[p + 1]; k++)
					std::cout << vals[k] << " ";
			}
		std::cout << std::endl;
	}
	MPI_Barrier(commGrid->getCommWorld());
	delete[] vals;
	delete[] sendcnt;
	delete[] sdispls;
}

template<typename V>
template<typename I>
void ColOwner<V>::debugPrint(const Range<I> &R) const {
	auto commGrid = R.getCommGrid();
	int size = commGrid->getSize();
	int *sendcnt = new int[size];
	int *sdispls = new int[size + 1];
	int length = c_size();
	MPI_Allgather(&length, 1, MPI_INT, sendcnt, 1, MPI_INT,
			commGrid->getCommWorld());
	sdispls[0] = 0;
	for (int i = 0; i < size; i++)
		sdispls[i + 1] = sdispls[i] + sendcnt[i];
	int total = sdispls[size];
	V *vals = new V[total];
	MPI_Datatype type = MPIType<V>();
	MPI_Gatherv(c_data(), length, type, vals, sendcnt, sdispls,
			type, 0, commGrid->getCommWorld());
	if (commGrid->getRank() == 0) {
		for (int i = 0; i < sdispls[size]; i++)
			std::cout << vals[i] << " ";
		std::cout << std::endl;
	}
	MPI_Barrier(commGrid->getCommWorld());
	delete[] vals;
	delete[] sendcnt;
	delete[] sdispls;
}

#endif /* DIST_VEC_H_ */
