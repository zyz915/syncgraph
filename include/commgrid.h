#ifndef COMMGRID_H_
#define COMMGRID_H_

#include <cassert>
#include <memory>
#include <mpi.h>

class CommGrid {
public:
	CommGrid(MPI_Comm world)
	{
		MPI_Comm_dup(world, &commWorld);
		MPI_Comm_rank(commWorld, &rank);
		MPI_Comm_size(commWorld, &size);

		int nRows = 1;
		while ((nRows + 1) * (nRows + 1) <= size) nRows++;
		assert(nRows * nRows == size);
		rows = cols = nRows;

		rowGroup = rank % nRows;
		colGroup = rank / nRows;

		MPI_Comm_split(commWorld, rowGroup, rank, &rowWorld);
		MPI_Comm_split(commWorld, colGroup, rank, &colWorld);

		MPI_Comm_rank(rowWorld, &rowRank);
		MPI_Comm_rank(colWorld, &colRank);

		assert( (rowRank == colGroup) );
		assert( (colRank == rowGroup) );
		assert( world != MPI_COMM_NULL );
	}

	int getRank() const { return rank; }
	int getSize() const { return size; }
	int getRowDim() const { return rows; }
	int getColDim() const { return cols; }

	int getRowGroup() const { return rowGroup; }
	int getColGroup() const { return colGroup; }

	int getPartner() const { return rowGroup * rows + colGroup; }

	const MPI_Comm& getCommWorld() const { return commWorld; }
	const MPI_Comm& getRowWorld() const { return rowWorld; }
	const MPI_Comm& getColWorld() const { return colWorld; }

private:
	MPI_Comm commWorld, rowWorld, colWorld;

	int size, rank;
	int rows, cols; // they are the same; (rows * cols == size)
	int rowGroup, rowRank;
	int colGroup, colRank;
};

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the k-th interval [start, end)
// * the i-th element is in the (i*p/n)-th interval
template<typename Index>
void getRange(Index n, int p, int k, Index &start, Index &end) {
	uint64_t t = (uint64_t) k * n + p - 1;
	start = t / p;
	end = (t + n) / p;
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the intervals in the array range
// * the i-th interval is [ range[i], range[i+1] )
template<typename Index>
void getRange(Index n, int p, Index *range) {
	uint64_t s = p - 1;
	for (int i = 0; i <= p; i++) {
		range[i] = s / p;
		s += n;
	}
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the length of the k-th interval
template<typename Index>
Index getLength(Index n, int p, int k) {
	uint64_t t = (uint64_t) k * n + p - 1;
	return (t + n) / p - t / p;
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the start index of the k-th interval
template<typename Index>
Index getStart(Index n, int p, int k) {
	return ((uint64_t) n * k + p - 1) / p;
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements, and return the end index of the k-th interval
template<typename Index>
Index getEnd(Index n, int p, int k) {
	return ((uint64_t) n * (k + 1) + p - 1) / p;
}

// * divide [0, n) into p intervals with roughly the same number
//   of elements. return the interval the given index belongs to
template<typename Index>
int getRank(Index n, int p, Index i) {
	return (int64_t) i * p / n;
}

template<typename Index = int>
class Range {
public:
	Range(CommGrid* cg, Index n):commGrid(cg), n(n)
	{
		getRange(n, commGrid->getRowDim(), commGrid->getRowGroup(), rStart, rEnd);
		rLength = rEnd - rStart;
		rOffset = getStart(rLength, commGrid->getColDim(), commGrid->getColGroup());
		rSize = getLength(rLength, commGrid->getColDim(), commGrid->getColGroup());

		getRange(n, commGrid->getColDim(), commGrid->getColGroup(), cStart, cEnd);
		cLength = cEnd - cStart;
		cOffset = getStart(cLength, commGrid->getRowDim(), commGrid->getRowGroup());
		cSize = getLength(cLength, commGrid->getRowDim(), commGrid->getRowGroup());
	}

	const CommGrid* getCommGrid() const {
		return commGrid;
	}

	// CommGrid
	CommGrid *commGrid;
	// total number of vertices
	Index n;
	// the elements stored in the row vector
	//   global index: [rStart, rEnd)
	//    local index: [0, rLength)
	Index rStart, rEnd;
	int rLength;
	// the elements stored in the column vector
	//   global index: [cStart, cEnd)
	//    local index: [0, cLength)
	Index cStart, cEnd;
	int cLength;
	// the elements owned by the row vector
	//   global index: [rStart + rOffset, rStart + rOffset + rSize)
	//    local index: [rOffset, rOffset + rSize)
	int rOffset, rSize;
	// the elements owned by the column vector
	//   global index: [cStart + cOffset, cStart + cOffset + cSize)
	//    local index: [cOffset, cOffset + cSize)
	int cOffset, cSize;
};

#endif /* COMMGRID_H_ */