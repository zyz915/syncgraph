#ifndef GLOBAL_H_
#define GLOBAL_H_

#include <sys/time.h>
#include <cctype>
#include <climits>
#include <mpi.h>

#ifdef _OPENMP
#include <omp.h>
#include <parallel/algorithm>
#include <parallel/numeric>

#define GET_OPENMP_THREAD_ID     omp_get_thread_num()
#define GET_OPENMP_NUM_THREAD    omp_get_num_threads()
#define SET_OPENMP_NUM_THREAD(x) omp_set_num_threads(x)
#define SET_OPENMP_DYNAMIC(x)    omp_set_dynamic(x)
#else
#define GET_OPENMP_THREAD_ID     (0)
#define GET_OPENMP_NUM_THREAD    (1)
#define SET_OPENMP_NUM_THREAD(x) 
#define SET_OPENMP_DYNAMIC(x)
#endif

#include "mpitype.h"

static int num_threads_;
inline int get_num_threads() {
	return num_threads_;
}

inline int get_thread_id() {
	return GET_OPENMP_THREAD_ID;
}

void set_num_threads(int x) {
	SET_OPENMP_DYNAMIC(0);
	SET_OPENMP_NUM_THREAD(x);
	#pragma omp parallel
	num_threads_ = GET_OPENMP_NUM_THREAD;
}

double getTimeSpan(struct timeval t1, struct timeval t2) {
	return
		(t2.tv_sec  - t1.tv_sec ) + 
		(t2.tv_usec - t1.tv_usec) * 1e-6;
}

template<typename T>
T allReduce(T v, MPI_Op op, MPI_Comm world) {
	T ret;
	MPI_Allreduce(&v, &ret, 1, MPIType<T>(), op, world);
	return ret;
}

#endif /* GLOBAL_H_ */