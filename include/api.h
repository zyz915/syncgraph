#ifndef API_H_
#define API_H_

#include <functional>

#include "accum.h"
#include "commgrid.h"
#include "compact_vec.h"
#include "constant_vec.h"
#include "csr_format.h"
#include "fully_dist_vec.h"
#include "graph.h"
#include "higher_order.h"

// TODO: make it an abstract class
template<typename V>
using SelectOp = std::function<bool(int, int, V)>;

template<typename V>
constexpr bool selectAll(int i, int j, V aij) {
	return true;
}

template<typename TY, typename TX>
class ACCUM {
public:
	virtual TY operator()(const TX &y, const TX &x) const = 0;
};

template<typename I = int>
class GraphAPI : public Range<I> {
public:
	GraphAPI(CommGrid* commGrid, I n);

	template<typename V>
	I numVertices(const Graph<I, V> &graph);

	template<typename V>
	int64_t numEdges(const Graph<I, V> &graph);

	template<typename V>
	void duplicate(const Graph<I, V> &src, Graph<I, V> &dst);

	template<typename TY, typename V, typename TX>
	void push(const CSRFormat<I, V> &graph, const ReadableRowVec<TX> &row,
			const ACCUM<TY, TX> &accum, WritableColVec<TY> &col);

	// more efficient push operation which completely ignores the matrix values
	template<typename TY, typename V, typename TX>
	void push(const Graph<I, V> &graph, const ReadableRowVec<TX> &row,
			const Monoid<TY, TX> &accum, WritableColVec<TY> &col);

	template<typename TY, typename V, typename TX, typename M>
	void push(const Graph<I, V> &graph, const ReadableRowVec<TX> &row,
			const ReadableRowVec<M> &mask, const Monoid<TY, TX> &accum, WritableColVec<TY> &col);

	// general push using the matrix values (via semiring)
	template<typename TY, typename V, typename TX>
	void push(const Graph<I, V> &graph, const ReadableRowVec<TX> &row,
			const Semiring<TY, V, TX> &sr, WritableColVec<TY> &col,
			SelectOp<V> op = selectAll<V>);

	template<typename TY, typename V, typename TX, typename M>
	void push(const Graph<I, V> &graph,
			const ReadableRowVec<TX> &row, const ReadableRowVec<M> &mask,
			const Semiring<TY, V, TX> &sr, WritableColVec<TY> &col,
			SelectOp<V> op = selectAll<V>);

	// push (result is sparse)
	template<typename T, typename V, typename M>
	void pushSparse(const Graph<I, V> &graph, const ReadableRowVec<M> &mask,
			const Monoid<T> &accum, FullyDistVec<I, T> &label);

	template<typename T, typename V, typename M>
	void pushSparse(const Graph<I, V> &graph, const ReadableRowVec<M> &mask,
			const Semiring<T, V> &sr, FullyDistVec<I, T> &label);

	// label propagation
	template<typename T, typename V>
	void propagate(const Graph<I, V> &graph,
			const Monoid<T> &accum, FullyDistVec<I, T> &label);

	template<typename T, typename V, typename M>
	void propagate(const Graph<I, V> &graph, FullyDistRowVec<I, M> &mask,
			const Monoid<T> &accum, FullyDistVec<I, T> &label);

	// general label propagation
	template<typename T, typename V>
	void propagate(const Graph<I, V> &graph,
			const Semiring<T, V> &sr, FullyDistVec<I, T> &label);

	template<typename T, typename V, typename M>
	void propagate(const Graph<I, V> &graph, FullyDistRowVec<I, M> &mask,
			const Semiring<T, V> &sr, FullyDistVec<I, T> &label);

	// reduce assign
	template<typename T>
	void reduceAssign(const ReadableRowVec<T> &row,
			const ReadableRowVec<I> &index,
			const Monoid<T> &accum, WritableColVec<T> &col);

	template<typename T>
	void reduceAssign(const ReadableColVec<T> &col,
			const ReadableColVec<I> &index,
			const Monoid<T> &accum, WritableRowVec<T> &row);

	// reduce extract
	template<typename T>
	void reduceExtract(const ReadableRowVec<T> &row,
			const ReadableColVec<I> &index,
			const Monoid<T> &accum, WritableColVec<T> &col);

	template<typename T>
	void reduceExtract(const ReadableColVec<T> &col,
			const ReadableRowVec<I> &index,
			const Monoid<T> &accum, WritableRowVec<T> &row);

	// extract (TODO): directly generate a compact vector
	template<typename T>
	void extract(const ReadableRowVec<T> &row,
			const ReadableColVec<I> &index,
			const Monoid<T> &accum, CompactColVec<I, T> &col);

	template<typename T>
	void extract(const ReadableColVec<T> &col,
			const ReadableRowVec<I> &index,
			const Monoid<T> &accum, CompactRowVec<I, T> &row);

	// reduce (graph -> vector)
	template<typename T, typename V>
	void reduce(const Graph<I, V> &graph, const Monoid<T> &accum,
			const UnaryOp<T, V> &op, WritableRowVec<T> &row);

	template<typename T, typename V>
	void reduce(const Graph<I, V> &graph, const Monoid<T> &accum,
			const UnaryOp<T, V> &op, WritableColVec<T> &col);

	// reduce (vector -> scalar)
	template<typename T>
	T reduce(const ReadableRowVec<T> &row, const Monoid<T> &monoid);

	template<typename T>
	T reduce(const ReadableColVec<T> &col, const Monoid<T> &monoid);

	// out-degree (special reduce)
	template<typename T, typename V>
	void degree(const Graph<I, V> &graph, WritableRowVec<T> &deg);

	// select subgraph
	template<typename V>
	void select(Graph<I, V> &graph, SelectOp<V> op);

	// debug print
	template<typename V>
	void debugPrint(const RowOwner<V> &row, const char *s);

	template<typename V>
	void debugPrint(const ColOwner<V> &col, const char *s);

	template<typename V>
	void debugPrint(const FullyDistVec<I, V> &v, const char *s);
};

template<typename I>
GraphAPI<I>::GraphAPI(CommGrid* commGrid, I n):
		Range<I>(commGrid, n) {}

template<typename I>
template<typename V>
I GraphAPI<I>::numVertices(const Graph<I, V> &graph)
{
	return graph.n;
}

template<typename I>
template<typename V>
int64_t GraphAPI<I>::numEdges(const Graph<I, V> &graph)
{
	return allReduce(graph.nnz, MPI_SUM, this->commGrid->getCommWorld());
}

template<typename I>
template<typename V>
void GraphAPI<I>::duplicate(const Graph<I, V> &src, Graph<I, V> &dst)
{
	dst.n = src.n;
	dst.nnz = src.nnz;
	dst.blocks = src.blocks;
	dst.dcsrarr = new DCSR_Block<I, V>[dst.blocks];
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads);
	for (int blk = 0; blk < dst.blocks; blk++) {
		const auto sb = src.getBlock(blk);
		auto db = dst.getBlock(blk);
		db->nzr = sb->nzr;
		db->nnz = sb->nnz;
		db->pos = new int64_t[db->nzr + 1];
		db->rowidx = new I[db->nzr];
		db->colidx = new I[db->nnz];
		db->values = new V[db->nnz];
		std::copy(sb->pos, sb->pos + sb->nzr + 1, db->pos);
		std::copy(sb->rowidx, sb->rowidx + sb->nzr, db->rowidx);
		std::copy(sb->colidx, sb->colidx + sb->nnz, db->colidx);
		std::copy(sb->values, sb->values + sb->nnz, db->values);
	}
}

template<typename I>
template<typename TY, typename V, typename TX>
void GraphAPI<I>::push(const CSRFormat<I, V> &csr, const ReadableRowVec<TX> &row,
		const ACCUM<TY, TX> &accum, WritableColVec<TY> &col)
{
	for (int u = 0; u < this->rLength; u++)
		for (int64_t i = csr.pos[u]; i < csr.pos[u + 1]; i++) {
			int v = csr.colidx[i];
			col[v] = accum(col[v], row[u]);
		}
	col.sync(MPI_MIN);
}


template<typename I>
template<typename TY, typename V, typename TX>
void GraphAPI<I>::push(const Graph<I, V> &graph, const ReadableRowVec<TX> &row,
		const Monoid<TY, TX> &accum, WritableColVec<TY> &col)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			int u = block->rowidx[i];
			for (int64_t j = block->pos[i]; j < block->pos[i + 1]; j++) {
				int v = block->colidx[j];
				accum.add(col[v], row[u]);
			}
		}
	}
	col.sync(accum.mpiOp());
}

template<typename I>
template<typename TY, typename V, typename TX, typename M>
void GraphAPI<I>::push(const Graph<I, V> &graph, const ReadableRowVec<TX> &row,
		const ReadableRowVec<M> &mask, const Monoid<TY, TX> &accum, WritableColVec<TY> &col)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			int u = block->rowidx[i];
			if (mask[u])
				for (int64_t j = block->pos[i]; j < block->pos[i + 1]; j++) {
					int v = block->colidx[j];
					accum.add(col[v], row[u]);
				}
		}
	}
	col.sync(accum.mpiOp());
}

template<typename I>
template<typename TY, typename V, typename TX>
void GraphAPI<I>::push(const Graph<I, V> &graph,
		const ReadableRowVec<TX> &row,
		const Semiring<TY, V, TX> &sr, WritableColVec<TY> &col,
		SelectOp<V> op)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			int u = block->rowidx[i];
			for (int64_t j = block->pos[i]; j < block->pos[i + 1]; j++) {
				int v = block->colidx[j];
				V   w = block->values[j];
				if (op(u, v, w))
					sr.axpy(col[v], w, row[u]);
			}
		}
	}
	col.sync(sr.mpiOp());
}

template<typename I>
template<typename TY, typename V, typename TX, typename M>
void GraphAPI<I>::push(const Graph<I, V> &graph,
		const ReadableRowVec<TX> &row, const ReadableRowVec<M> &mask,
		const Semiring<TY, V, TX> &sr, WritableColVec<TY> &col,
		SelectOp<V> op)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		int tid = blk % nthreads;
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			int u = block->rowidx[i];
			if (mask[u])
				for (int64_t j = block->pos[i]; j < block->pos[i + 1]; j++) {
					int v = block->colidx[j];
					V   w = block->values[j];
					if (op(u, v, w))
						sr.axpy(col[v], w, row[u]);
				}
		}
	}
	col.sync(sr.mpiOp());
}

template<typename I>
template<typename T, typename V, typename M>
void GraphAPI<I>::pushSparse(const Graph<I, V> &graph,
		const ReadableRowVec<M> &mask, const Monoid<T> &accum,
		FullyDistVec<I, T> &label)
{
	int nthreads = get_num_threads();
	WritableRowVec<T> &row = label.row();
	WritableColVec<T> &col = label.col();
	std::vector<I> inds[nthreads];
	std::vector<T> vals[nthreads];
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		int tid = blk % nthreads;
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			int u = block->rowidx[i];
			if (mask[u])
				for (int64_t j = block->pos[i]; j < block->pos[i + 1]; j++) {
					int v = block->colidx[j];
					T   y = col[v];
					accum.add(y, row[u]);
					if (y != col[v]) {
						inds[tid].push_back(v);
						vals[tid].push_back(y);
					}
				}
		}
	}
	I tot = 0;
	for (int tid = 0; tid < nthreads; tid++)
		tot += inds[tid].size();
	I *indBuf = new I[tot];
	T *valBuf = new T[tot];
	tot = 0;
	for (int tid = 0; tid < nthreads; tid++) {
		std::copy(inds[tid].begin(), inds[tid].end(), indBuf + tot);
		std::copy(vals[tid].begin(), vals[tid].end(), valBuf + tot);
		tot += inds[tid].size();
	}
	label.syncKeyVal(indBuf, valBuf, tot, accum);
	delete[] indBuf;
	delete[] valBuf;
}

template<typename I>
template<typename T, typename V, typename M>
void GraphAPI<I>::pushSparse(const Graph<I, V> &graph,
		const ReadableRowVec<M> &mask, const Semiring<T, V> &sr,
		FullyDistVec<I, T> &label)
{
	int nthreads = get_num_threads();
	WritableRowVec<T> &row = label.row();
	WritableColVec<T> &col = label.col();
	std::vector<I> inds[nthreads];
	std::vector<T> vals[nthreads];
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		int tid = blk % nthreads;
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			int u = block->rowidx[i];
			if (mask[u])
				for (int64_t j = block->pos[i]; j < block->pos[i + 1]; j++) {
					I v = block->colidx[j];
					V w = block->values[j];
					T y = col[v];
					sr.axpy(y, row[u], w);
					if (y != col[v]) {
						inds[tid].push_back(v);
						vals[tid].push_back(y);
					}
				}
		}
	}
	I tot = 0;
	for (int tid = 0; tid < nthreads; tid++)
		tot += inds[tid].size();
	I *indBuf = new I[tot];
	T *valBuf = new T[tot];
	tot = 0;
	for (int tid = 0; tid < nthreads; tid++) {
		std::copy(inds[tid].begin(), inds[tid].end(), indBuf + tot);
		std::copy(vals[tid].begin(), vals[tid].end(), valBuf + tot);
		tot += inds[tid].size();
	}
	label.syncKeyVal(indBuf, valBuf, tot, sr);
	delete[] indBuf;
	delete[] valBuf;
}

template<typename I>
template<typename T, typename V>
void GraphAPI<I>::propagate(const Graph<I, V> &graph,
		const Monoid<T> &accum, FullyDistVec<I, T> &label)
{
	FullyDistRowVec<I, int> mask(*this);
	mask.fill(1);
	propagate(graph, mask, accum, label);
}

template<typename I>
template<typename T, typename V>
void GraphAPI<I>::propagate(const Graph<I, V> &graph,
		const Semiring<T, V> &sr, FullyDistVec<I, T> &label)
{
	FullyDistRowVec<I, int> mask(*this);
	mask.fill(1);
	propagate(graph, mask, sr, label);
}

template<typename I>
template<typename T, typename V, typename M>
void GraphAPI<I>::propagate(const Graph<I, V> &graph, FullyDistRowVec<I, M> &mask, 
		const Monoid<T> &accum, FullyDistVec<I, T> &label)
{
	ConstantRowVec<M> one(static_cast<M>(1));
	I activeCnt = reduce(zip(mask, one, EQ<I, M>()), PLUS<I>());
	I threshold = static_cast<I>(graph.n * this->cLength / graph.nnz);
	FullyDistRowVec<I, T> &row = label.row();
	FullyDistColVec<I, T> &col = label.col();
	FullyDistRowVec<I, T> dup(row);
	while (activeCnt) {
		if (activeCnt * 10 > this->rLength) {
			push(graph, row, mask, accum, col);
			copyColToRow(col, row);
		} else {
			pushSparse(graph, mask, accum, label);
		}
		mask = zip(row, dup, NE<M, T>());
		activeCnt = reduce(zip(mask, one, EQ<I, M>()), PLUS<I>());
		dup = row;
	}
}

template<typename I>
template<typename T, typename V, typename M>
void GraphAPI<I>::propagate(const Graph<I, V> &graph, FullyDistRowVec<I, M> &mask, 
		const Semiring<T, V> &sr, FullyDistVec<I, T> &label)
{
	ConstantRowVec<M> one(static_cast<M>(1));
	I activeCnt = reduce(zip(mask, one, EQ<I, M>()), PLUS<I>());
	I threshold = static_cast<I>(graph.n * this->cLength / graph.nnz);
	FullyDistRowVec<I, T> &row = label.row();
	FullyDistColVec<I, T> &col = label.col();
	FullyDistRowVec<I, T> dup(row);
	while (activeCnt) {
		if (activeCnt * 10 > this->rLength) {
			push(graph, row, mask, sr, col);
			copyColToRow(col, row);
		} else {
			pushSparse(graph, mask, sr, label);
		}
		mask = zip(row, dup, NE<M, T>());
		activeCnt = reduce(zip(mask, one, EQ<I, M>()), PLUS<I>());
		dup = row;
	}
}

template<typename I>
template<typename T>
void GraphAPI<I>::reduceAssign(const ReadableRowVec<T> &row, const ReadableRowVec<I> &index,
		const Monoid<T> &accum, WritableColVec<T> &col)
{
	for (int i = 0; i < this->rLength; i++)
		if (index[i] >= this->cStart && index[i] < this->cEnd) {
			int j = index[i] - this->cStart;
			accum.add(col[j], row[i]);
		}
	col.sync(accum.mpiOp());
}

template<typename I>
template<typename T>
void GraphAPI<I>::reduceAssign(const ReadableColVec<T> &col, const ReadableColVec<I> &index,
		const Monoid<T> &accum, WritableRowVec<T> &row)
{
	for (int i = 0; i < this->cLength; i++)
		if (index[i] >= this->rStart && index[i] < this->rEnd) {
			int j = index[i] - this->rStart;
			accum.add(row[j], col[i]);
		}
	row.sync(accum.mpiOp());
}

template<typename I>
template<typename T>
void GraphAPI<I>::reduceExtract(const ReadableRowVec<T> &row, const ReadableColVec<I> &index,
		const Monoid<T> &accum, WritableColVec<T> &col)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < this->cLength; i++)
		if (index[i] >= this->rStart && index[i] < this->rEnd) {
			int j = index[i] - this->rStart;
			accum.add(col[i], row[j]);
		}
	col.sync(accum.mpiOp());
}

template<typename I>
template<typename T>
void GraphAPI<I>::reduceExtract(const ReadableColVec<T> &col, const ReadableRowVec<I> &index,
		const Monoid<T> &accum, WritableRowVec<T> &row)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int i = 0; i < this->rLength; i++)
		if (index[i] >= this->cStart && index[i] < this->cEnd) {
			int j = index[i] - this->cStart;
			accum.add(row[i], col[j]);
		}
	row.sync(accum.mpiOp());
}

template<typename I>
template<typename T, typename V>
void GraphAPI<I>::reduce(const Graph<I, V> &graph,
		const Monoid<T> &accum, const UnaryOp<T, V> &op,
		WritableRowVec<T> &row)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			I u = block->rowidx[i];
			for (int j = block->pos[i]; j < block->pos[i + 1]; j++) {
				V w = block->values[j];
				accum.add(row[u], op(w));
			}
		}
	}
	row.sync(accum.mpiOp());
}

template<typename I>
template<typename T, typename V>
void GraphAPI<I>::reduce(const Graph<I, V> &graph,
		const Monoid<T> &accum, const UnaryOp<T, V> &op,
		WritableColVec<T> &col)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			I u = block->rowidx[i];
			for (int j = block->pos[i]; j < block->pos[i + 1]; j++) {
				I v = block->colidx[j];
				V w = block->values[j];
				accum.add(col[v], op(w));
			}
		}
	}
	col.sync(accum.mpiOp());
}

template<typename I>
template<typename T>
T GraphAPI<I>::reduce(const ReadableRowVec<T> &row, const Monoid<T> &monoid) {
	int start = this->rOffset, end = this->rOffset + this->rSize;
	T val = monoid.id();
	for (int i = start; i < end; i++)
		monoid.add(val, row[i]);
	return allReduce(val, monoid.mpiOp(), this->getCommGrid()->getCommWorld());
}

template<typename I>
template<typename T>
T GraphAPI<I>::reduce(const ReadableColVec<T> &col, const Monoid<T> &monoid) {
	int start = this->cOffset, end = this->cOffset + this->cSize;
	T val = monoid.id();
	for (int i = start; i < end; i++)
		monoid.add(val, col[i]);
	return allReduce(val, monoid.mpiOp(), this->getCommGrid()->getCommWorld());
}

template<typename I>
template<typename T, typename V>
void GraphAPI<I>::degree(const Graph<I, V> &graph, WritableRowVec<T> &deg)
{
	int nthreads = get_num_threads();
	deg.set(ConstantRowVec<T>(0));
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		auto block = graph.getBlock(blk);
		for (int i = 0; i < block->nzr; i++) {
			I u = block->rowidx[i];
			I delta = block->pos[i + 1] - block->pos[i];
			__sync_fetch_and_add(& deg[u], delta);
		}
	}
	deg.sync(MPI_SUM);
}

template<typename I>
template<typename V>
void GraphAPI<I>::select(Graph<I, V> &graph, SelectOp<V> op)
{
	int nthreads = get_num_threads();
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		auto block = graph.getBlock(blk);
		int nzr = 0;
		int64_t ptr = 0;
		for (int i = 0; i < block->nzr; i++) {
			I u = block->rowidx[i];
			int64_t start = ptr;
			for (int64_t j = block->pos[i]; j < block->pos[i + 1]; j++) {
				I v = block->colidx[j];
				V w = block->values[j];
				if (op(u, v, w)) {
					block->colidx[ptr] = v;
					block->values[ptr] = w;
					ptr++;
				}
			}
			if (ptr > start) {
				block->rowidx[nzr] = u;
				block->pos[nzr] = start;
				nzr++;
			}
		}
		block->nzr = nzr;
		block->nnz = ptr;
		block->pos[nzr] = ptr;
	}
	graph.nnz = 0;
	for (int blk = 0; blk < graph.blocks; blk++)
		graph.nnz += graph.dcsrarr[blk].nnz;
}

template<typename I>
template<typename V>
void GraphAPI<I>::debugPrint(const RowOwner<V> &row, const char *s) {
	if (this->commGrid->getRank() == 0)
		std::cout << s << ": ";
	row.debugPrint(*this);
}

template<typename I>
template<typename V>
void GraphAPI<I>::debugPrint(const ColOwner<V> &col, const char *s) {
	if (this->commGrid->getRank() == 0)
		std::cout << s << ": ";
	col.debugPrint(*this);
}

template<typename I>
template<typename V>
void GraphAPI<I>::debugPrint(const FullyDistVec<I, V> &v, const char *s) {
	if (this->commGrid->getRank() == 0)
		std::cout << s << ": ";
	v.col().debugPrint(*this);
}

#endif /* API_H_ */
