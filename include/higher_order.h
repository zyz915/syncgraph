#ifndef HIGHER_ORDER_H_
#define HIGHER_ORDER_H_

#include "accum.h"
#include "dist_vec.h"

/**
 * Intermediate data structures to support higher-order functions:
 *
 *        Vec    UnaryOp     Vec
 * map :: [a] -> (a -> b) -> [b]
 *
 *        Vec    Vec    BinaryOp         Vec
 * zip :: [a] -> [b] -> (a -> b -> c) -> [c]
 *
 */

template<typename R, typename V>
class MapRowVec : public ReadableRowVec<R> {
public:
	MapRowVec(const ReadableRowVec<V> &a,
			const UnaryOp<R, V> &op):
			vec_(a), op_(op) {}

	R operator[](size_t i) const override {
		return op_.apply(vec_[i]);
	}

private:
	const ReadableRowVec<V> &vec_;
	const UnaryOp<R, V> &op_;
};

template<typename R, typename V>
class MapColVec : public ReadableColVec<R> {
public:
	MapColVec(const ReadableColVec<V> &a,
			const UnaryOp<R, V> &op):
			vec_(a), op_(op) {}

	R operator[](size_t i) const override {
		return op_.apply(vec_[i]);
	}

private:
	const ReadableColVec<V> &vec_;
	const UnaryOp<R, V> &op_;
};

template<typename R, typename V>
class MapDistVec : public ReadableVec<R> {
public:
	MapDistVec(const ReadableVec<V> &a, const UnaryOp<R, V> &op):
			row_(a.row(), op), col_(a.col(), op) {}

	const ReadableRowVec<R>& row() const override { return row_; }
	const ReadableColVec<R>& col() const override { return col_; }

private:
	MapRowVec<R, V> row_;
	MapColVec<R, V> col_;
};

template<typename R, typename V1, typename V2>
class ZipRowVec : public ReadableRowVec<R> {
public:
	ZipRowVec(const ReadableRowVec<V1> &lhs,
			const ReadableRowVec<V2> &rhs,
			const BinaryOp<R, V1, V2> &op):
			lhs_(lhs), rhs_(rhs), op_(op) {}

	R operator[](size_t i) const override {
		return op_.mult(lhs_[i], rhs_[i]);
	}

private:
	const ReadableRowVec<V1> &lhs_;
	const ReadableRowVec<V2> &rhs_;
	const BinaryOp<R, V1, V2> &op_;
};

template<typename R, typename V1, typename V2>
class ZipColVec : public ReadableColVec<R> {
public:
	ZipColVec(const ReadableColVec<V1> &lhs,
			const ReadableColVec<V2> &rhs,
			const BinaryOp<R, V1, V2> &op):
			lhs_(lhs), rhs_(rhs), op_(op) {}

	R operator[](size_t i) const override {
		return op_.mult(lhs_[i], rhs_[i]);
	}

private:
	const ReadableColVec<V1> &lhs_;
	const ReadableColVec<V2> &rhs_;
	const BinaryOp<R, V1, V2> &op_;
};

template<typename R, typename V1, typename V2>
class ZipDistVec : public ReadableVec<R> {
public:
	ZipDistVec(const ReadableVec<V1> &lhs, const ReadableVec<V2> &rhs, const BinaryOp<R, V1, V2> &op):
			row_(lhs.row(), rhs.row(), op), col_(lhs.col(), rhs.col(), op) {}

	const ReadableRowVec<R>& row() const override { return row_; }
	const ReadableColVec<R>& col() const override { return col_; }

private:
	ZipRowVec<R, V1, V2> row_;
	ZipColVec<R, V1, V2> col_;
};

template<typename R, typename V>
MapRowVec<R, V> map(const ReadableRowVec<V> &a, const UnaryOp<R, V> &op) {
	return MapRowVec<R, V>(a, op);
}

template<typename R, typename V>
MapColVec<R, V> map(const ReadableColVec<V> &a, const UnaryOp<R, V> &op) {
	return MapColVec<R, V>(a, op);
}

template<typename R, typename V>
MapDistVec<R, V> map(const ReadableVec<V> &a, const UnaryOp<R, V> &op) {
	return MapDistVec<R, V>(a, op);
}

template<typename R, typename V1, typename V2>
ZipRowVec<R, V1, V2> zip(const ReadableRowVec<V1> &a, const ReadableRowVec<V2> &b,
		const BinaryOp<R, V1, V2> &op) {
	return ZipRowVec<R, V1, V2>(a, b, op);
}

template<typename R, typename V1, typename V2>
ZipColVec<R, V1, V2> zip(const ReadableColVec<V1> &a, const ReadableColVec<V2> &b,
		const BinaryOp<R, V1, V2> &op) {
	return ZipColVec<R, V1, V2>(a, b, op);
}

template<typename R, typename V1, typename V2>
ZipDistVec<R, V1, V2> zip(const ReadableVec<V1> &a, const ReadableVec<V2> &b,
		const BinaryOp<R, V1, V2> &op) {
	return ZipDistVec<R, V1, V2>(a, b, op);
}

#endif /* HIGH_ORDER_H_ */
