#ifndef ACCUM_H_
#define ACCUM_H_

#include <mpi.h>
#include <algorithm>
#include <limits>

template<typename T, typename T1>
class UnaryOp {
public:
	UnaryOp() = default;
	UnaryOp(const UnaryOp<T, T1> &it) = default;

	virtual T apply(const T1 &a) const = 0;
};

template<typename T, typename T1 = T>
class Monoid {
public:
	Monoid() = default;
	Monoid(const Monoid<T, T1> &it) = default;

	virtual T id() const = 0;
	virtual void add(T &a, const T1 &b) const = 0;
	virtual MPI_Op mpiOp() const = 0;
};

template<typename T, typename T1, typename T2>
class BinaryOp {
public:
	BinaryOp() = default;
	BinaryOp(const BinaryOp<T, T1, T2> &it) = default;

	virtual T mult(const T1 &a, const T2 &b) const = 0;
};

template<typename TY, typename V, typename TX = TY>
class Semiring {
public:
	Semiring(const Monoid<TY> &m, const BinaryOp<TY, V, TX> &b):
			monoid_(m), bop_(b) {}

	operator const Monoid<TY>&() const {
		return monoid_;
	}

	virtual void axpy(TY &y, const V &a, const TX &x) const {
		monoid_.add(y, bop_.mult(a, x));
	}

	virtual MPI_Op mpiOp() const {
		return monoid_.mpiOp();
	}

private:
	const Monoid<TY> &monoid_;
	const BinaryOp<TY, V, TX> &bop_;
};

template<typename T>
class NOT : public UnaryOp<T, T> {
public:
	T apply(const T &a) const override {
		return !a;
	}
};

template<typename T>
class NEG : public UnaryOp<T, T> {
public:
	T apply(const T &a) const override {
		return -a;
	}
};

template<typename T, typename V>
class CAST : public UnaryOp<T, V> {
public:
	T apply(const V &a) const override {
		return static_cast<T>(a);
	}
};

template<typename T>
class MIN : public Monoid<T, T>, public BinaryOp<T, T, T> {
public:
	T id() const override {
		return std::numeric_limits<T>::max();
	}

	T mult(const T &a, const T &b) const override {
		return std::min(a, b);
	}

	void add(T &a, const T &b) const override {
		if (b < a) a = b;
	}

	MPI_Op mpiOp() const override {
		return MPI_MIN;
	}
};

template<typename T>
class MAX : public Monoid<T, T>, public BinaryOp<T, T, T> {
public:
	T id() const override {
		return std::numeric_limits<T>::min();
	}

	T mult(const T &a, const T &b) const override {
		return std::max(a, b);
	}

	void add(T &a, const T &b) const override {
		if (b > a) a = b;
	}

	MPI_Op mpiOp() const override {
		return MPI_MAX;
	}
};

template<typename T>
class PLUS : public Monoid<T, T>, public BinaryOp<T, T, T> {
public:
	T id() const override {
		return static_cast<T>(0);
	}

	T mult(const T &a, const T &b) const override {
		return a + b;
	}

	void add(T &a, const T &b) const override {
		a += b;
	}

	MPI_Op mpiOp() const override {
		return MPI_SUM;
	}
};

template<typename T>
class MINUS : public BinaryOp<T, T, T> {
public:
	T mult(const T &a, const T &b) const override {
		return a - b;
	}
};

template<typename T>
class TIMES : public Monoid<T, T>, public BinaryOp<T, T, T> {
public:
	T id() const override {
		return static_cast<T>(1);
	}

	T mult(const T &a, const T &b) const override {
		return a * b;
	}

	void add(T &a, const T &b) const override {
		a *= b;
	}

	MPI_Op mpiOp() const override {
		return MPI_PROD;
	}
};

template<typename T>
class DIV : public BinaryOp<T, T, T> {
public:
	T mult(const T &a, const T &b) const override {
		return a / b;
	}
};

template<typename T, typename V1 = T, typename V2 = V1>
class LAND : public Monoid<T, T>, public BinaryOp<T, V1, V2> {
public:
	T id() const override {
		return static_cast<T>(true);
	}

	T mult(const V1 &a, const V2 &b) const override {
		return static_cast<T>(a && b);
	}

	void add(T &a, const T &b) const override {
		a &= b;
	}

	MPI_Op mpiOp() const override {
		return MPI_LAND;
	}
};

template<typename T, typename V1 = T, typename V2 = V1>
class LOR : public Monoid<T, T>, public BinaryOp<T, V1, V2> {
public:
	T id() const override {
		return static_cast<T>(false);
	}

	T mult(const V1 &a, const V2 &b) const override {
		return static_cast<T>(a || b);
	}

	void add(T &a, const T &b) const override {
		a |= b;
	}

	MPI_Op mpiOp() const override {
		return MPI_LOR;
	}
};

template<typename T, typename V1 = T, typename V2 = V1>
class EQ : public BinaryOp<T, V1, V2> {
public:
	T mult(const V1 &a, const V2 &b) const {
		return static_cast<T>(a == b);
	}
};

template<typename T, typename V1 = T, typename V2 = V1>
class NE : public BinaryOp<T, V1, V2> {
public:
	T mult(const V1 &a, const V2 &b) const {
		return static_cast<T>(a != b);
	}
};

template<typename T, typename V>
class LT : public BinaryOp<T, V, V> {
public:
	T mult(const V &a, const V &b) const {
		return static_cast<T>(a < b);
	}
};

template<typename T, typename V>
class LE : public BinaryOp<T, V, V> {
public:
	T mult(const V &a, const V &b) const {
		return static_cast<T>(a <= b);
	}
};

template<typename T, typename V>
class GT : public BinaryOp<T, V, V> {
public:
	T mult(const V &a, const V &b) const {
		return static_cast<T>(a > b);
	}
};

template<typename T, typename V>
class GE : public BinaryOp<T, V, V> {
public:
	T mult(const V &a, const V &b) const {
		return static_cast<T>(a >= b);
	}
};


#endif /* ACCUM_H_ */