#ifndef FULLY_DIST_VEC_H_
#define FULLY_DIST_VEC_H_

#include "accum.h"
#include "commgrid.h"
#include "constant_vec.h"
#include "dist_vec.h"
#include "global.h"
#include "mpitype.h"

/**
 * FullyDistRowVec, FullyDistColVec:
 *  - distributed row/column vectors with actual storage
 *
 * FullyDistVec:
 *  - store both the distributed row/column vectors
 *  - copy the contents from one to another
 */

template<typename I, typename V>
class FullyDistRowVec : public RowOwner<V>,
		public ReadableRowVec<V>, public WritableRowVec<V> {
public:
	// constructors & deconstructor
	FullyDistRowVec(const Range<I> &R);
	FullyDistRowVec(const FullyDistRowVec<I, V> &row);
	~FullyDistRowVec();
	// move semantics
	FullyDistRowVec(FullyDistRowVec<I, V> &&row);

	// array's local array & length
	V* getPtr();
	const V* getPtr() const;
	size_t length() const;
	// access the elements by local index
	V& operator[](size_t i) override;
	V operator[](size_t i) const override;

	// data owned by the current process
	V* r_data() override;
	const V* r_data() const override;
	// size of owned data
	int r_size() const override;

	// swap (be careful when using its C pointer elsewhere)
	void swap(FullyDistRowVec<I, V> &it);

	// assignment
	FullyDistRowVec<I, V>& operator=(const FullyDistRowVec<I, V> &it);
	FullyDistRowVec<I, V>& operator=(const ConstantRowVec<V> &it);
	FullyDistRowVec<I, V>& operator=(const ReadableRowVec<V> &it);
	// assignment with a mask
	template<typename M>
	void set(const ReadableRowVec<M> &mask, const ReadableRowVec<V> &it);
	void set(const ReadableRowVec<V> &it) override { *this = it; }

	// comparison
	bool operator==(const FullyDistRowVec<I, V> &it) const;
	bool operator==(const ReadableRowVec<V> &it) const;
	bool operator==(const RowOwner<V> &it) const;

	bool operator!=(const FullyDistRowVec<I, V> &it) const;
	bool operator!=(const ReadableRowVec<V> &it) const;
	bool operator!=(const RowOwner<V> &it) const;

	// reduce
	template<typename T>
	T reduce(const Monoid<T, V> &monoid) const;
	template<typename T, typename M>
	T reduce(const ReadableRowVec<M> &mask, const Monoid<T, V> &monoid) const;

	// set/get an element by the global index
	void setElement(I index, const V &val);
	V getElement(I index) const;
	// fill the vector with a specified value
	void fill(const V &val);

	// synchronize the values (by row-wise allreduce)
	void sync(MPI_Op op) override;

	// free the allocated memory
	void freeMemory();

	const CommGrid* getCommGrid() const { return R_.getCommGrid(); }

protected:
	const Range<I> &R_;
	V *ptr_;
};

template<typename I, typename V>
class FullyDistColVec : public ColOwner<V>,
		public ReadableColVec<V>, public WritableColVec<V> {
public:
	// constructors & deconstructor
	FullyDistColVec(const Range<I> &R);
	FullyDistColVec(const FullyDistColVec<I, V> &row);
	~FullyDistColVec();
	// move semantics
	FullyDistColVec(FullyDistColVec<I, V> &&row);

	// array's local array & length
	V* getPtr();
	const V* getPtr() const;
	size_t length() const;
	// access the elements by local index
	V& operator[](size_t i) override;
	V operator[](size_t i) const override;

	// data owned by the current process
	V* c_data() override;
	const V* c_data() const override;
	// size of owned data
	int c_size() const override;


	// swap (be careful when using its C pointer elsewhere)
	void swap(FullyDistColVec<I, V> &it);

	// assignment
	FullyDistColVec<I, V>& operator=(const FullyDistColVec<I, V> &it);
	FullyDistColVec<I, V>& operator=(const ConstantColVec<V> &it);
	FullyDistColVec<I, V>& operator=(const ReadableColVec<V> &it);
	// assignment with a mask
	template<typename M>
	void set(const ReadableColVec<M> &mask, const ReadableColVec<V> &it);
	void set(const ReadableColVec<V> &it) override { *this = it; }

	// comparison
	bool operator==(const FullyDistColVec<I, V> &it) const;
	bool operator==(const ReadableColVec<V> &it) const;
	bool operator==(const ColOwner<V> &it) const;

	bool operator!=(const FullyDistColVec<I, V> &it) const;
	bool operator!=(const ReadableColVec<V> &it) const;
	bool operator!=(const ColOwner<V> &it) const;

	// reduce
	template<typename T>
	T reduce(const Monoid<T, V> &monoid) const;
	template<typename T, typename M>
	T reduce(const ReadableColVec<M> &mask, const Monoid<T, V> &monoid) const;

	// set/get an element by the global index
	void setElement(I index, const V &val);
	V getElement(I index) const;
	// fill the vector with a specified value
	void fill(const V &val);

	// synchronize the values (by column-wise allreduce)
	void sync(MPI_Op op) override;

	// free the allocated memory
	void freeMemory();

	const CommGrid* getCommGrid() const { return R_.getCommGrid(); }

private:
	const Range<I> &R_;
	V *ptr_;
};

template<typename I, typename V>
void copyRowToCol(const FullyDistRowVec<I, V> &row, FullyDistColVec<I, V> &col) {
	auto commGrid = row.getCommGrid();
	const V *rPtr = row.getPtr();
	V *cPtr = col.getPtr();
	MPI_Status status;
	if (commGrid->getRowGroup() != commGrid->getColGroup()) {
		int partner = commGrid->getPartner();
		MPI_Sendrecv(
			rPtr, row.length(), MPIType<V>(), partner, 0,
			cPtr, col.length(), MPIType<V>(), partner, 0,
			commGrid->getCommWorld(), &status);
	} else
		std::copy(rPtr, rPtr + row.length(), cPtr);
}

template<typename I, typename V>
void copyColToRow(const FullyDistColVec<I, V> &col, FullyDistRowVec<I, V> &row) {
	auto commGrid = col.getCommGrid();
	const V *cPtr = col.getPtr();
	V *rPtr = row.getPtr();
	MPI_Status status;
	if (commGrid->getRowGroup() != commGrid->getColGroup()) {
		int partner = commGrid->getPartner();
		MPI_Sendrecv(
			cPtr, col.length(), MPIType<V>(), partner, 0,
			rPtr, row.length(), MPIType<V>(), partner, 0,
			commGrid->getCommWorld(), &status);
	} else
		std::copy(cPtr, cPtr + col.length(), rPtr);
}

template<typename I, typename V>
class FullyDistVec : public ReadableVec<V> {
public:
	// constructors & deconstructor
	FullyDistVec(const Range<I> &R);
	FullyDistVec(const FullyDistVec<I, V> &v);
	~FullyDistVec() {}
	// move semantics
	FullyDistVec(const FullyDistVec<I, V> &&v);

	// swap (be careful when using its C pointer elsewhere)
	void swap(FullyDistVec<I, V> &it);

	// assignment
	FullyDistVec<I, V>& operator=(const FullyDistVec<I, V> &it);
	FullyDistVec<I, V>& operator=(const ReadableVec<V> &it);

	// comparison
	bool operator==(const FullyDistVec<I, V> &it) const;
	bool operator==(const ReadableVec<V> &it) const;

	bool operator!=(const FullyDistVec<I, V> &it) const;
	bool operator!=(const ReadableVec<V> &it) const;

	// reduce
	template<typename T>
	T reduce(const Monoid<T, V> &monoid) const;
	template<typename T, typename M>
	T reduce(const FullyDistVec<I, M> &mask, const Monoid<T, V> &monoid) const;

	// count the # of occurrence of an element
	I count(const V &val) const;

	// set/get an element by the global index
	void setElement(I index, const V &val);
	V getElement(I index) const;
	// fill the vector with a specified value
	void fill(const V &val);

	// get the row/col vector
	// const ReadableRowVec<V>& row() const override { return row_; }
	// const ReadableColVec<V>& col() const override { return col_; }

	FullyDistRowVec<I, V>& row() { return row_; }
	FullyDistColVec<I, V>& col() { return col_; }

	const FullyDistRowVec<I, V>& row() const { return row_; }
	const FullyDistColVec<I, V>& col() const { return col_; }

	// get the C array of the row/col vector
	V *getRowPtr() { return row_.getPtr(); }
	const V *getRowPtr() const { return row_.getPtr(); }
	V *getColPtr() { return col_.getPtr(); }
	const V *getColPtr() const { return col_.getPtr(); }

	// update the dense column vector with a sparse vector
	// cnt is converted to int32_t (due to the MPI limitation)
	void syncKeyVal(const I *inds, const V *vals, int cnt, const Monoid<V> &monoid);

	// synchronize the values
	// column-wise allreduce + copy from column to row
	void syncColToRow(MPI_Op op);
	// row-wise allreduce + copy from row to column
	void syncRowToCol(MPI_Op op);

	// free the allocated memory
	void freeMemory();
	// debug print
	void debugPrint() const;

private:
	const Range<I> &R_;
	// row / col arrays
	FullyDistRowVec<I, V> row_;
	FullyDistColVec<I, V> col_;
};

//--------------- FullyDistRowVec ---------------

template<typename I, typename V>
FullyDistRowVec<I, V>::FullyDistRowVec(const Range<I> &R):R_(R) {
	ptr_ = new V[R_.rLength];
}

template<typename I, typename V>
FullyDistRowVec<I, V>::FullyDistRowVec(const FullyDistRowVec<I, V> &row):R_(row.R_) {
	ptr_ = new V[R_.rLength];
	std::copy(row.ptr_, row.ptr_ + R_.rLength, ptr_);
}

template<typename I, typename V>
FullyDistRowVec<I, V>::~FullyDistRowVec() {
	if (ptr_ != nullptr)
		delete[] ptr_;
}

template<typename I, typename V>
FullyDistRowVec<I, V>::FullyDistRowVec(FullyDistRowVec<I, V> &&row) {
	std::swap(ptr_, row.ptr_);
}

template<typename I, typename V>
V* FullyDistRowVec<I, V>::getPtr() {
	return ptr_;
}

template<typename I, typename V>
const V* FullyDistRowVec<I, V>::getPtr() const {
	return ptr_;
}

template<typename I, typename V>
size_t FullyDistRowVec<I, V>::length() const {
	return R_.rLength;
}

template<typename I, typename V>
V& FullyDistRowVec<I, V>::operator[](size_t i) {
	return ptr_[i];
}

template<typename I, typename V>
V FullyDistRowVec<I, V>::operator[](size_t i) const {
	return ptr_[i];
}

template<typename I, typename V>
V* FullyDistRowVec<I, V>::r_data() {
	return ptr_ + R_.rOffset;
}

template<typename I, typename V>
const V* FullyDistRowVec<I, V>::r_data() const {
	return ptr_ + R_.rOffset;
}

template<typename I, typename V>
int FullyDistRowVec<I, V>::r_size() const {
	return R_.rSize;
}

template<typename I, typename V>
void FullyDistRowVec<I, V>::swap(FullyDistRowVec<I, V> &it) {
	std::swap(ptr_, it.ptr_);
}

template<typename I, typename V>
FullyDistRowVec<I, V>& FullyDistRowVec<I, V>::operator=(const FullyDistRowVec<I, V> &it) {
	std::copy(it.ptr_, it.ptr_ + R_.rLength, ptr_);
	return *this;
}

template<typename I, typename V>
FullyDistRowVec<I, V>& FullyDistRowVec<I, V>::operator=(const ConstantRowVec<V> &it) {
	std::fill(it.ptr_, it.ptr_ + R_.rLength, it.getValue());
	return *this;
}

template<typename I, typename V>
FullyDistRowVec<I, V>& FullyDistRowVec<I, V>::operator=(const ReadableRowVec<V> &it) {
	for (int i = 0; i < R_.rLength; i++)
		ptr_[i] = it[i];
	return *this;
}

template<typename I, typename V>
bool FullyDistRowVec<I, V>::operator==(const FullyDistRowVec<I, V> &it) const {
	int eq = !memcmp(ptr_ + R_.rOffset, it.ptr_ + R_.rOffset, sizeof(V) * R_.rSize);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool FullyDistRowVec<I, V>::operator==(const ReadableRowVec<V> &it) const {
	int eq = 1;
	for (int i = R_.rOffset; eq && i < R_.rOffset + R_.rSize; i++)
		eq = (ptr_[i] == it[i]);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool FullyDistRowVec<I, V>::operator==(const RowOwner<V> &it) const {
	int eq = !memcmp(ptr_ + R_.rOffset, it.r_data(), sizeof(V) * R_.rSize);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool FullyDistRowVec<I, V>::operator!=(const FullyDistRowVec<I, V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
bool FullyDistRowVec<I, V>::operator!=(const ReadableRowVec<V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
bool FullyDistRowVec<I, V>::operator!=(const RowOwner<V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
template<typename M>
void FullyDistRowVec<I, V>::set(const ReadableRowVec<M> &mask, const ReadableRowVec<V> &it) {
	for (int i = 0; i < R_.rLength; i++)
		if (mask[i])
			ptr_[i] = it[i];
}

template<typename I, typename V>
template<typename T>
T FullyDistRowVec<I, V>::reduce(const Monoid<T, V> &monoid) const {
	T val = monoid.id();
	for (int i = R_.rOffset; i < R_.rOffset + R_.rSize; i++)
		monoid.add(val, ptr_[i]);
	return allReduce(val, monoid.mpiOp(), R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
template<typename T, typename M>
T FullyDistRowVec<I, V>::reduce(const ReadableRowVec<M> &mask, const Monoid<T, V> &monoid) const {
	T val = monoid.id();
	for (int i = R_.rOffset; i < R_.rOffset + R_.rSize; i++)
		if (mask[i])
			monoid.add(val, ptr_[i]);
	return allReduce(val, monoid.mpiOp(), R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
void FullyDistRowVec<I, V>::setElement(I index, const V &val) {
	if (index >= R_.rStart && index < R_.rEnd)
		ptr_[index - R_.rStart] = val;
}

template<typename I, typename V>
V FullyDistRowVec<I, V>::getElement(I index) const {
	int dim = R_.getCommGrid()->getRowDim();
	int rank = getRank(R_.n, dim, index);
	V val;
	if (index >= R_.rStart && index < R_.rEnd)
		val = ptr_[index - R_.rStart];
	MPI_Bcast(&val, 1, MPIType<V>(), rank, R_.getCommGrid()->getRowWorld());
	return val;
}

template<typename I, typename V>
void FullyDistRowVec<I, V>::fill(const V &val) {
	std::fill(ptr_, ptr_ + R_.rLength, val);
}

template<typename I, typename V>
void FullyDistRowVec<I, V>::sync(MPI_Op op) {
	MPI_Allreduce(MPI_IN_PLACE, ptr_, R_.rLength, MPIType<V>(), op,
			R_.getCommGrid()->getRowWorld());
}

template<typename I, typename V>
void FullyDistRowVec<I, V>::freeMemory() {
	if (ptr_ != nullptr) {
		delete[] ptr_;
		ptr_ = nullptr;
	}
}

//--------------- FullyDistColVec ---------------

template<typename I, typename V>
FullyDistColVec<I, V>::FullyDistColVec(const Range<I> &R):R_(R) {
	ptr_ = new V[R_.cLength];
}

template<typename I, typename V>
FullyDistColVec<I, V>::FullyDistColVec(const FullyDistColVec<I, V> &col):R_(col.R_) {
	ptr_ = new V[R_.cLength];
	std::copy(col.ptr_, col.ptr_ + R_.cLength, ptr_);
}

template<typename I, typename V>
FullyDistColVec<I, V>::~FullyDistColVec() {
	if (ptr_ != nullptr)
		delete[] ptr_;
}

template<typename I, typename V>
FullyDistColVec<I, V>::FullyDistColVec(FullyDistColVec<I, V> &&col) {
	std::swap(ptr_, col.ptr_);
}

template<typename I, typename V>
V* FullyDistColVec<I, V>::getPtr() {
	return ptr_;
}

template<typename I, typename V>
const V* FullyDistColVec<I, V>::getPtr() const {
	return ptr_;
}

template<typename I, typename V>
size_t FullyDistColVec<I, V>::length() const {
	return R_.cLength;
}

template<typename I, typename V>
V& FullyDistColVec<I, V>::operator[](size_t i) {
	return ptr_[i];
}

template<typename I, typename V>
V FullyDistColVec<I, V>::operator[](size_t i) const {
	return ptr_[i];
}

template<typename I, typename V>
V* FullyDistColVec<I, V>::c_data() {
	return ptr_ + R_.cOffset;
}

template<typename I, typename V>
const V* FullyDistColVec<I, V>::c_data() const {
	return ptr_ + R_.cOffset;
}

template<typename I, typename V>
int FullyDistColVec<I, V>::c_size() const {
	return R_.cSize;
}

template<typename I, typename V>
FullyDistColVec<I, V>& FullyDistColVec<I, V>::operator=(const FullyDistColVec<I, V> &it) {
	std::copy(it.ptr_, it.ptr_ + R_.cLength, ptr_);
	return *this;
}

template<typename I, typename V>
FullyDistColVec<I, V>& FullyDistColVec<I, V>::operator=(const ConstantColVec<V> &it) {
	std::fill(it.ptr_, it.ptr_ + R_.cLength, it.getValue());
	return *this;
}

template<typename I, typename V>
FullyDistColVec<I, V>& FullyDistColVec<I, V>::operator=(const ReadableColVec<V> &it) {
	for (int i = 0; i < R_.cLength; i++)
		ptr_[i] = it[i];
	return *this;
}

template<typename I, typename V>
template<typename M>
void FullyDistColVec<I, V>::set(const ReadableColVec<M> &mask, const ReadableColVec<V> &it) {
	for (int i = 0; i < R_.cLength; i++)
		if (mask[i])
			ptr_[i] = it[i];
}

template<typename I, typename V>
void FullyDistColVec<I, V>::swap(FullyDistColVec<I, V> &it) {
	std::swap(ptr_, it.ptr_);
}

template<typename I, typename V>
bool FullyDistColVec<I, V>::operator==(const FullyDistColVec<I, V> &it) const {
	int eq = !memcmp(ptr_ + R_.cOffset, it.ptr_ + R_.cOffset, sizeof(V) * R_.cSize);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool FullyDistColVec<I, V>::operator==(const ReadableColVec<V> &it) const {
	int eq = 1;
	for (int i = R_.cOffset; eq && i < R_.cOffset + R_.cSize; i++)
		eq = (ptr_[i] == it[i]);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool FullyDistColVec<I, V>::operator==(const ColOwner<V> &it) const {
	int eq = !memcmp(ptr_ + R_.cOffset, it.c_data(), sizeof(V) * R_.cSize);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool FullyDistColVec<I, V>::operator!=(const FullyDistColVec<I, V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
bool FullyDistColVec<I, V>::operator!=(const ReadableColVec<V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
bool FullyDistColVec<I, V>::operator!=(const ColOwner<V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
template<typename T>
T FullyDistColVec<I, V>::reduce(const Monoid<T, V> &monoid) const {
	T val = monoid.id();
	for (int i = R_.cOffset; i < R_.cOffset + R_.cSize; i++)
		monoid.add(val, ptr_[i]);
	return allReduce(val, monoid.mpiOp(), R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
template<typename T, typename M>
T FullyDistColVec<I, V>::reduce(const ReadableColVec<M> &mask, const Monoid<T, V> &monoid) const {
	T val = monoid.id();
	for (int i = R_.cOffset; i < R_.cOffset + R_.cSize; i++)
		if (mask[i])
			monoid.add(val, ptr_[i]);
	return allReduce(val, monoid.mpiOp(), R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
void FullyDistColVec<I, V>::setElement(I index, const V &val) {
	if (index >= R_.cOffset && index < R_.cOffset + R_.cSize)
		ptr_[index - R_.cOffset] = val;
}

template<typename I, typename V>
V FullyDistColVec<I, V>::getElement(I index) const {
	int dim = R_.getCommGrid()->getColDim();
	int rank = getRank(R_.n, dim, index);
	V val;
	if (index >= R_.cStart && index < R_.cEnd)
		val = ptr_[index - R_.cSize];
	MPI_Bcast(&val, 1, MPIType<V>(), rank, R_.getCommGrid()->getColWorld());
	return val;
}

template<typename I, typename V>
void FullyDistColVec<I, V>::fill(const V &val) {
	std::fill(ptr_, ptr_ + R_.cLength, val);
}

template<typename I, typename V>
void FullyDistColVec<I, V>::sync(MPI_Op op) {
	MPI_Allreduce(MPI_IN_PLACE, ptr_, R_.cLength, MPIType<V>(), op,
			R_.getCommGrid()->getColWorld());
}

template<typename I, typename V>
void FullyDistColVec<I, V>::freeMemory() {
	if (ptr_ != nullptr) {
		delete[] ptr_;
		ptr_ = nullptr;
	}
}

//--------------- FullyDistVec ---------------

template<typename I, typename V>
FullyDistVec<I, V>::FullyDistVec(const Range<I> &R):
		row_(R), col_(R), R_(R)
{
	// the current implementation assumes that the process grid is square
	assert(R_.getCommGrid()->getRowDim() == R_.getCommGrid()->getColDim());
}

template<typename I, typename V>
FullyDistVec<I, V>::FullyDistVec(const FullyDistVec<I, V> &it):R_(it.R_),
		row_(it.row_), col_(it.col_) {}

template<typename I, typename V>
void FullyDistVec<I, V>::swap(FullyDistVec<I, V> &it) {
	row_.swap(it.row_);
	col_.swap(it.col_);
}

template<typename I, typename V>
FullyDistVec<I, V>& FullyDistVec<I, V>::operator=(const FullyDistVec<I, V> &it) {
	row_ = it.row_;
	col_ = it.col_;
	return *this;
}

template<typename I, typename V>
FullyDistVec<I, V>& FullyDistVec<I, V>::operator=(const ReadableVec<V> &it) {
	row_ = it.row();
	col_ = it.col();
	return *this;
}

template<typename I, typename V>
bool FullyDistVec<I, V>::operator==(const FullyDistVec<I, V> &it) const {
	return row_ == it.row_;
}

template<typename I, typename V>
bool FullyDistVec<I, V>::operator!=(const FullyDistVec<I, V> &it) const {
	return row_ != it.row_;
}

template<typename I, typename V>
template<typename T>
T FullyDistVec<I, V>::reduce(const Monoid<T, V> &monoid) const {
	return row_.reduce(monoid);
}

template<typename I, typename V>
template<typename T, typename M>
T FullyDistVec<I, V>::reduce(const FullyDistVec<I, M> &mask, const Monoid<T, V> &monoid) const {
	return row_.reduce(mask.row(), monoid);
}

template<typename I, typename V>
I FullyDistVec<I, V>::count(const V &val) const {
	return row_.count(val);
}

template<typename I, typename V>
void FullyDistVec<I, V>::setElement(I index, const V &val) {
	row_.setElement(index, val);
	col_.setElement(index, val);
}

template<typename I, typename V>
V FullyDistVec<I, V>::getElement(I index) const {
	return row_.getElement(index);
}

template<typename I, typename V>
void FullyDistVec<I, V>::fill(const V &val) {
	row_.fill(val);
	col_.fill(val);
}

template<typename I, typename V>
void FullyDistVec<I, V>::syncKeyVal(const I *inds, const V *vals, int cnt, const Monoid<V> &monoid) {
	V *rPtr = getRowPtr();
	V *cPtr = getColPtr();
	int rows = R_.getCommGrid()->getRowDim();
	int *sendcnt = new int[rows];
	int *sdispls = new int[rows + 1];
	MPI_Allgather(&cnt, 1, MPI_INT, sendcnt, 1, MPI_INT,
			R_.getCommGrid()->getColWorld());
	sdispls[0] = 0;
	for (int i = 0; i < rows; i++)
		sdispls[i + 1] = sdispls[i] + sendcnt[i];
	int total = sdispls[rows];
	I *sendIdx = new I[total];
	V *sendVal = new V[total];
	MPI_Allgatherv(inds, cnt, MPIType<I>(), sendIdx, sendcnt, sdispls,
			MPIType<I>(), R_.getCommGrid()->getColWorld());
	MPI_Allgatherv(vals, cnt, MPIType<V>(), sendVal, sendcnt, sdispls,
			MPIType<V>(), R_.getCommGrid()->getColWorld());
	// modify the column array
	for (int i = 0; i < total; i++)
		cPtr[sendIdx[i]] = std::min(cPtr[sendIdx[i]], sendVal[i]);
	// modify the row array
	if (R_.getCommGrid()->getRowGroup() != R_.getCommGrid()->getColGroup()) {
		int partner = R_.getCommGrid()->getPartner();
		int tot_send = total, tot_recv; // received bytes from partner
		MPI_Status status;
		MPI_Sendrecv(
			&tot_send, 1, MPI_INT, partner, 0,
			&tot_recv, 1, MPI_INT, partner, 0,
			R_.getCommGrid()->getCommWorld(), &status);
		I *recvIdx = new I[tot_recv];
		V *recvVal = new V[tot_recv];
		MPI_Sendrecv(
			sendIdx, tot_send, MPIType<I>(), partner, 0,
			recvIdx, tot_recv, MPIType<I>(), partner, 0,
			R_.getCommGrid()->getCommWorld(), &status);
		MPI_Sendrecv(
			sendVal, tot_send, MPIType<I>(), partner, 0,
			recvVal, tot_recv, MPIType<I>(), partner, 0,
			R_.getCommGrid()->getCommWorld(), &status);
		// modify the column array
		for (int i = 0; i < tot_recv; i++)
			monoid.add(rPtr[recvIdx[i]], recvVal[i]);
		delete[] recvIdx;
		delete[] recvVal;
	} else
		std::copy(cPtr, cPtr + col_.length(), rPtr);
	delete[] sendcnt;
	delete[] sdispls;
	delete[] sendIdx;
	delete[] sendVal;
}

template<typename I, typename V>
void FullyDistVec<I, V>::syncColToRow(MPI_Op op) {
	col_.sync(op);
	copyColToRow(col_, row_);
}

template<typename I, typename V>
void FullyDistVec<I, V>::syncRowToCol(MPI_Op op) {
	row_.sync(op);
	copyRowToCol(row_, col_);
}

template<typename I, typename V>
void FullyDistVec<I, V>::freeMemory() {
	row_.freeMemory();
	col_.freeMemory();
}

template<typename I, typename V>
void FullyDistVec<I, V>::debugPrint() const {
	col_.debugPrint(R_);
}

#endif /* FULLY_DIST_VEC_H_ */