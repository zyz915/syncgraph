#ifndef GRAPH_H_
#define GRAPH_H_

#include <cstdlib>
#include <mpi.h>
#include <iostream>
#include <sstream>
#include <vector>
#include <sys/stat.h>

#include "global.h"

using namespace std;

// vertices are indexed from [0..n-1]
// typename I: type of the index
// typename V: type of the value
template<typename I, typename V>
struct DCSR_Block {
	I nzr;            // non-zero rows
	int64_t nnz;      // non-zero elements (assumed to exceed the range of int)
	I *rowidx;        // I   [ nzr ]   : indexes of each non-zero row (local index)
	int64_t *pos;     // I64 [ nzr+1 ] : the range in the 'colidx' belonging to each non-zero row
	I *colidx;        // I   [ nnz ]   : column indexes (local index)
	V *values;        // V   [ nnz ]   : matrix values

	DCSR_Block();
	DCSR_Block(const DCSR_Block<I, V> &it) = delete;

	void freeMemory();
};

template<typename I, typename V>
struct Graph {
	I n;              // matrix dimension
	int64_t nnz;      // non-zero elements (assumed to exceed the range of int)
	int blocks;       // number of blocks
	DCSR_Block<I, V> *dcsrarr;

	Graph();
	Graph(const Graph<I, V> &it) = delete;

	DCSR_Block<I, V>* getBlock(int i) { return dcsrarr + i; }
	const DCSR_Block<I, V>* getBlock(int i) const { return dcsrarr + i; }

	void freeMemory();
};

template<typename I, typename V>
DCSR_Block<I, V>::DCSR_Block():pos(nullptr),
		rowidx(nullptr), colidx(nullptr), values(nullptr) {}

template<typename I, typename V>
void DCSR_Block<I, V>::freeMemory() {
	if (pos != nullptr) {
		delete[] pos;
		delete[] rowidx;
		delete[] colidx;
		delete[] values;		
	}
	pos = nullptr;
	rowidx = nullptr;
	colidx = nullptr;
	values = nullptr;
}

template<typename I, typename V>
Graph<I, V>::Graph():dcsrarr(nullptr) {}

template<typename I, typename V>
void Graph<I, V>::freeMemory() {
	if (dcsrarr != nullptr) {
		for (int blk = 0; blk < blocks; blk++)
			dcsrarr[blk].freeMemory();
		delete[] dcsrarr;
	}
	dcsrarr = nullptr;
}

template<typename I, typename V>
struct EdgeUnit {
	I src, dst;
	V weight;
	EdgeUnit() = default;
	EdgeUnit(const EdgeUnit<I, V> &it) = default;
	EdgeUnit(I s, I d, V w):src(s), dst(d), weight(w) {}

	bool operator<(const EdgeUnit &it) const {
		return (src != it.src ? src < it.src : dst < it.dst);
	}
};

struct Param {
	int type = 0; // 0 = matrix, 1 = binary
	const char *fname = "";
	int n = 0;
	int nthreads = 1;
	int symm = 0;
};

void usage(const char *argv0)
{
	printf("usage: %s [OPTIONS]\n", argv0);
	printf("  the options are:\n");
	printf("    -I <string>   filename\n");
	printf("    -F <string>   input format ('mtx' or 'bin')\n");
	printf("    -N <int>      number of vertices (required for 'bin')\n");
	printf("    -T <int>      number of threads  (default: 1)\n");
	printf("    -S <int>      convert to symmetric graph (default: 0)\n");
	exit(0);	
}

Param parseParam(int argc, char **argv) {
	Param param;
	int p = 1;
	for (; p + 1 < argc; p += 2) {
		if (!strcmp(argv[p], "-I")) {
			param.fname = argv[p + 1];
		} else
		if (!strcmp(argv[p], "-F")) {
			if (!strcmp(argv[p + 1], "mtx"))
				param.type = 0;
			else if (!strcmp(argv[p + 1], "bin"))
				param.type = 1;
			else {
				printf("invalid input format\n");
				usage(argv[0]);
			}
		} else
		if (!strcmp(argv[p], "-N")) {
			param.n = atoi(argv[p + 1]);
		} else
		if (!strcmp(argv[p], "-T")) {
			param.nthreads = atoi(argv[p + 1]);
		} else
		if (!strcmp(argv[p], "-S")) {
			param.symm = atoi(argv[p + 1]);
		} else {
			printf("unrecognized flag: %s\n", argv[p]);
			usage(argv[0]);
		}
	}
	if (strlen(param.fname) == 0) {
		printf("filename unspecified\n");
		usage(argv[0]);
	}
	if (param.type == 1 && param.n == 0) {
		printf("require the number of vertices for binary format\n");
		usage(argv[0]);
	}
	set_num_threads(param.nthreads);
	return param;
}

static const int BUFF_LEN = 1024;
static char buff[BUFF_LEN];

static void reportError(int rank, const char *msg) {
	if (rank == 0)
		printf("%s\n", msg);
	exit(0);
}

template<typename T>
static void exchange(CommGrid *commGrid, vector<T> &sendbuf, vector<T> &recvbuf)
{
	MPI_Status status;
	int elem = sizeof(T);
	MPI_Datatype type;
	MPI_Type_contiguous(elem, MPI_CHAR, &type);
	MPI_Type_commit(&type);

	int pt = commGrid->getPartner();
	int sendcnt = sendbuf.size(), recvcnt;
	MPI_Sendrecv(
			&sendcnt, 1, MPI_INT, pt, 0,
			&recvcnt, 1, MPI_INT, pt, 0,
			commGrid->getCommWorld(), &status);
	T *ptr = new T[recvcnt];
	MPI_Sendrecv(
			sendbuf.data(), sendcnt, type, pt, 0,
			ptr, recvcnt, type, pt, 0,
			commGrid->getCommWorld(), &status);
	sendbuf.clear();
	size_t curr = recvbuf.size();
	recvbuf.resize(curr + recvcnt);
	copy(ptr, ptr + recvcnt, recvbuf.begin() + curr);
	MPI_Type_free(&type);
	delete[] ptr;
}

template<typename T>
static void all2all(CommGrid *commGrid, vector<vector<T> > &sendbuf, vector<vector<T> > &recvbuf)
{
	int np = commGrid->getSize();
	int me = commGrid->getRank();
	int *sendcnt = new int[np];
	int *recvcnt = new int[np];

	for (int i = 0; i < np; i++)
		sendcnt[i] = sendbuf[i].size();
	//MPI_Alltoall(sendcnt, 1, MPI_INT, recvcnt, 1, MPI_INT, MPI_COMM_WORLD);

	MPI_Status status;
	int elem = sizeof(T);
	MPI_Datatype type;
	MPI_Type_contiguous(elem, MPI_CHAR, &type);
	MPI_Type_commit(&type);

	for (int i = 1; i < np; i++) {
		int fr = (me + np - i) % np;
		int to = (me + i) % np;
		MPI_Sendrecv(
				&sendcnt[to], 1, MPI_INT, to, 0,
				&recvcnt[fr], 1, MPI_INT, fr, 0,
				MPI_COMM_WORLD, &status);
		recvbuf[fr].resize(recvcnt[fr]);
		MPI_Sendrecv(
				sendbuf[to].data(), sendcnt[to], type, to, 0,
				recvbuf[fr].data(), recvcnt[fr], type, fr, 0,
				MPI_COMM_WORLD, &status);
		sendbuf[to].clear();
	}
	recvbuf[me].swap(sendbuf[me]);

	long long totsend = 0;
	for (int i = 0; i < np; i++)
		totsend += sendcnt[i];

	delete[] sendcnt;
	delete[] recvcnt;
	MPI_Type_free(&type);
}

template<typename T>
T parse(char *p);

template<>
bool parse(char *p) {
	return false;
}

template<>
int parse(char *p) {
	int x = 0, neg = 0;
	while (isspace(*p)) p++;
	if (*p == '-') {
		neg = 1;
		p++;
	}
	do {
		x = (x << 1) + (x << 3) + (*(p++) - '0');
	} while (isdigit(*p));
	return (neg ? -x : x);
}

template<>
unsigned parse(char *p) {
	unsigned x = 0;
	while (isspace(*p)) p++;
	do {
		x = (x << 1) + (x << 3) + (*(p++) - '0');
	} while (isdigit(*p));
	return x;
}

template<>
double parse(char *p) {
	int x = 0, neg = 0;
	double y = 0.0, pow10 = 1.0;
	while (isspace(*p)) p++;
	if (*p == '-') {
		neg = 1;
		p++;
	}
	do {
		x = (x << 1) + (x << 3) + (*(p++) - '0');
	} while (isdigit(*p));
	if (*p != '.')
		return (neg ? -x : x);
	p++;
	while (isdigit(*p)) {
		pow10 *= 0.1;
		y += pow10 * (*(p--) - '0');
	}
	return (neg ? -(x + y) : x + y);
}

template<typename I, typename V>
static void tuple2Block(const Range<I> &R, const vector<EdgeUnit<I, V> > &elist,
		DCSR_Block<I, V> &block)
{
	block.nzr = (elist.empty() ? 0 : 1);
	for (int64_t i = 1; i < elist.size(); i++)
		if (elist[i].src != elist[i - 1].src)
			block.nzr += 1;
	block.nnz = elist.size();
	block.rowidx = new I[block.nzr];
	block.pos = new int64_t[block.nzr + 1];
	block.colidx = new I[block.nnz];
	block.values = new V[block.nnz];
	block.pos[0] = 0;
	if (elist.empty()) return;
	I next = 1;
	block.rowidx[0] = elist[0].src - R.rStart;
	block.colidx[0] = elist[0].dst - R.cStart;
	block.values[0] = elist[0].weight;
	for (int64_t i = 1; i < elist.size(); i++) {
		if (elist[i].src != elist[i - 1].src) {
			block.rowidx[next] = elist[i].src - R.rStart;
			block.pos[next] = i;
			next++;
		}
		block.colidx[i] = elist[i].dst - R.cStart;
		block.values[i] = elist[i].weight;
	}
	block.pos[block.nzr] = block.nnz;
	assert(next == block.nzr);
}

template<typename I, typename V>
static void tuple2Graph(const Range<I> &R, vector<EdgeUnit<I, V> > &recv,
		Graph<I, V> &graph)
{
	int nthreads = get_num_threads();
	graph.n = R.n;
	graph.nnz = recv.size();
	graph.blocks = nthreads * 4;
	vector<vector<EdgeUnit<I, V> > > elist(graph.blocks);
	for (int64_t i = 0; i < recv.size(); i++) {
		int r = getRank(R.cLength, graph.blocks, recv[i].dst - R.cStart);
		elist[r].push_back(recv[i]);
	}
	recv.clear();
	graph.dcsrarr = new DCSR_Block<I, V>[graph.blocks];
	#pragma omp parallel for num_threads(nthreads);
	for (int blk = 0; blk < graph.blocks; blk++) {
		sort(elist[blk].begin(), elist[blk].end());
		tuple2Block(R, elist[blk], graph.dcsrarr[blk]);
		elist[blk].clear();
	}
}

template<typename I, typename V>
static void graph2Tuple(const Range<I> &R, const Graph<I, V> &graph,
		vector<EdgeUnit<I, V> > &send)
{
	int nthreads = get_num_threads();
	vector<EdgeUnit<I, V> > elist[nthreads];
	#pragma omp parallel for num_threads(nthreads)
	for (int blk = 0; blk < graph.blocks; blk++) {
		int tid = blk % nthreads;
		auto block = graph.getBlock(blk);
		for (I i = 0; i < block->nzr; i++) {
			I u = R.rStart + block->rowidx[i];
			for (I j = block->pos[i]; j < block->pos[i + 1]; j++) {
				I v = R.cStart + block->colidx[j];
				V w = block->values[j];
				elist[tid].push_back(EdgeUnit<I, V>(u, v, w));
			}
		}
	}
	for (int tid = 0; tid < nthreads; tid++) {
		send.insert(send.end(), elist[tid].begin(), elist[tid].end());
		elist[tid].clear();
	}
}

// matrix market exchange format
// ignore the matrix values
template<typename I, typename V>
void loadMatrix(CommGrid *commGrid, Param &param, Graph<I, V> &graph, bool weighted)
{
	FILE *f = fopen(param.fname, "r");
	if (f == NULL) {
		sprintf(buff, "error! cannot open %s", param.fname);
		reportError(commGrid->getRank(), buff);
	}

	while (fgets(buff, BUFF_LEN, f))
		if (*buff != '%') break;

	I r, c;
	int64_t nnz;
	stringstream ss(buff);
	ss >> r >> c >> nnz;
	if (r != c) {
		sprintf(buff, "%s is not a square matrix!", param.fname);
		reportError(commGrid->getRank(), buff);
	}
	if (commGrid->getRank() == 0)
		cout << "|V| " << r << " |E| " <<  nnz << endl;

	I n = param.n = r;
	int dim = commGrid->getRowDim();
	int rank = commGrid->getRank();
	int nprocs = commGrid->getSize();
	Range<I> R(commGrid, n);
	vector<EdgeUnit<I, V> > send, recv;

	while (fgets(buff, BUFF_LEN, f)) {
		I x = 0, y = 0;
		char *p = buff;
		do {
			x = (x << 3) + (x << 1) + ((*p++) - '0');
		} while (isdigit(*p));
		p++;
		do {
			y = (y << 3) + (y << 1) + ((*p++) - '0');
		} while (isdigit(*p));
		x--; y--;
		V z = (weighted ? parse<V>(p) : 0);
		if (x >= R.rStart && x < R.rEnd && y >= R.cStart && y < R.cEnd) {
			recv.push_back(EdgeUnit<I, V>(x, y, z));
			if (param.symm)
				send.push_back(EdgeUnit<I, V>(y, x, z));
		}
	}
	fclose(f);

	if (param.symm)
		exchange(commGrid, send, recv);
	tuple2Graph(R, recv, graph);
}

// binary format storing a sequence of edge tuples
//   x1, y1, x2, y2, x3, y3 .. xn, yn
// each number is a 4-byte unsigned int
// all the values are 0-based.
template<typename I, typename V>
void loadBinaryUnweighted(CommGrid *commGrid, Param &param, Graph<I, V> &graph)
{
	const int CHUNKSIZE = 1 << 24;
	I n = param.n;
	int nprocs = commGrid->getSize();
	Range<I> R(commGrid, n);
	vector<EdgeUnit<I, V> > send, recv;
	int dim = commGrid->getRowDim();
	int rank = commGrid->getRank();

	FILE *f = fopen(param.fname, "rb");
	if (f == NULL) {
		sprintf(buff, "error! cannot open %s\n", param.fname);
		reportError(rank, buff);
	}
	struct stat st;
	long fsize = 0;
	if (stat(param.fname, &st) == 0)
		fsize = st.st_size;
	// read the graph
	int unitSize = sizeof(I) * 2;
	long end = fsize / unitSize, ptr = 0;
	I *es = new I[CHUNKSIZE * 2];
	while (ptr < end) {
		int cnt = min(CHUNKSIZE, (int)(end - ptr));
		int actual = fread(es, unitSize, cnt, f);
		for (int i = 0; i < cnt; i++) {
			I x = es[i * 2];
			I y = es[i * 2 + 1];
			V z = 0;
			if (x >= R.rStart && x < R.rEnd && y >= R.cStart && y < R.cEnd) {
				recv.push_back(EdgeUnit<I, V>(x, y, z));
				if (param.symm)
					send.push_back(EdgeUnit<I, V>(y, x, z));
			}
		}
		ptr += cnt;
	}
	delete[] es;
	fclose(f);

	if (param.symm)
		exchange(commGrid, send, recv);
	tuple2Graph(R, recv, graph);
}

// binary format storing a sequence of edge tuples
//   x1, y1, x2, y2, x3, y3 .. xn, yn
// each number is a 4-byte unsigned int
// all the values are 0-based.
template<typename I, typename V>
void loadBinaryWeighted(CommGrid *commGrid, Param &param, Graph<I, V> &graph)
{
	const int CHUNKSIZE = 1 << 24;
	I n = param.n;
	int nprocs = commGrid->getSize();
	Range<I> R(commGrid, n);
	vector<EdgeUnit<I, V> > send, recv;
	int dim = commGrid->getRowDim();
	int rank = commGrid->getRank();

	FILE *f = fopen(param.fname, "rb");
	if (f == NULL) {
		sprintf(buff, "error! cannot open %s\n", param.fname);
		reportError(rank, buff);
	}
	struct stat st;
	long fsize = 0;
	if (stat(param.fname, &st) == 0)
		fsize = st.st_size;
	// read the graph
	int unitSize = sizeof(I) * 3;
	long end = fsize / unitSize, ptr = 0;
	I *es = new I[CHUNKSIZE * 3];
	while (ptr < end) {
		int cnt = min(CHUNKSIZE, (int)(end - ptr));
		int actual = fread(es, unitSize, cnt, f);
		for (int i = 0; i < cnt; i++) {
			I x = es[i * 3];
			I y = es[i * 3 + 1];
			V z = 0;
			if (x >= R.rStart && x < R.rEnd && y >= R.cStart && y < R.cEnd) {
				recv.push_back(EdgeUnit<I, V>(x, y, z));
				if (param.symm)
					send.push_back(EdgeUnit<I, V>(y, x, z));
			}
		}
		ptr += cnt;
	}
	delete[] es;
	fclose(f);

	if (param.symm)
		exchange(commGrid, send, recv);
	tuple2Graph(R, recv, graph);
}

// load the transposed graph as a matrix in Graph format
template<typename I, typename V>
void loadGraph(CommGrid *commGrid, Param &param, Graph<I, V> &graph, bool weighted = false)
{
	if (commGrid->getRank() == 0)
		printf("filename: %s\n", param.fname);
	double t1 = MPI_Wtime();
	switch (param.type) {
		case 0: {
			loadMatrix(commGrid, param, graph, weighted);
			break;
		}
		case 1: {
			loadBinaryUnweighted(commGrid, param, graph);
			break;
		}
		case 2: {
			loadBinaryWeighted(commGrid, param, graph);
			break;
		}
	}
	MPI_Barrier(commGrid->getCommWorld());
	double t2 = MPI_Wtime();
	if (commGrid->getRank() == 0)
		printf("finish reading: %fs\n", t2 - t1);
}

template<typename I, typename V>
void loadWeightedGraph(CommGrid *commGrid, Param &param, Graph<I, V> &graph)
{
	return loadGraph(commGrid, param, graph, true);
}

template<typename I, typename V>
void transpose(Range<I> &R, const Graph<I, V> &inp,
		Graph<I, V> &out)
{
	const CommGrid *commGrid = R.getCommGrid();
	out.freeMemory();
	out.n = inp.n;
	out.blocks = inp.blocks;
	// dump the graph into tuples
	vector<EdgeUnit<I, V> > send, recv;
	graph2Tuple(R, inp, send);
	// swap the row/col index
	for (int i = 0; i < send.size(); i++)
		swap(send[i].src, send[i].dst);
	// exchange the tuples
	int elem = sizeof(EdgeUnit<I, V>);
	MPI_Datatype type;
	MPI_Type_contiguous(elem, MPI_CHAR, &type);
	MPI_Type_commit(&type);
	int partner = commGrid->getPartner();
	// 1. exchage nnz
	MPI_Status status;
	MPI_Sendrecv(
		&inp.nnz, 1, MPIType<int64_t>(), partner, 0,
		&out.nnz, 1, MPIType<int64_t>(), partner, 0,
		commGrid->getCommWorld(), &status);
	// 2. exchange data
	recv.resize(out.nnz);
	MPI_Sendrecv(
		send.data(), inp.nnz, type, partner, 0,
		recv.data(), out.nnz, type, partner, 0,
		commGrid->getCommWorld(), &status);
	send.clear();
	// convert tuples to graph
	tuple2Graph(R, recv, out);
	MPI_Type_free(&type);
}

#endif /* GRAPH_H_ */
