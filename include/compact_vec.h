#ifndef COMPACT_VEC_H_
#define COMPACT_VEC_H_

#include "constant_vec.h"
#include "dist_vec.h"
#include "fully_dist_vec.h"

/**
 * Space-efficient implementation of the distributed row/column vector.
 * Each process stores the elements it owned (no replication).
 *
 * CompactRowVec, CompactColVec:
 *  - implementation of the distributed row/column owner vector
 */

template<typename I, typename V>
class CompactRowVec : public RowOwner<V> {
public:
	// constructor & deconstructor
	CompactRowVec(const Range<I> &R);
	CompactRowVec(const CompactRowVec<I, V> &it);
	~CompactRowVec();

	// get the C array pointer
	V* r_data() override;
	const V* r_data() const override;
	// size of owned data
	int r_size() const override;

	// swap
	void swap(CompactRowVec<I, V> &it);

	// assignment
	CompactRowVec<I, V>& operator=(const FullyDistRowVec<I, V> &it);
	CompactRowVec<I, V>& operator=(const ReadableRowVec<V> &it);
	CompactRowVec<I, V>& operator=(const RowOwner<V> &it);

	// comparison
	bool operator==(const FullyDistRowVec<I, V> &it) const;
	bool operator==(const ReadableRowVec<V> &it) const;
	bool operator==(const RowOwner<V> &it) const;

	bool operator!=(const FullyDistRowVec<I, V> &it) const;
	bool operator!=(const ReadableRowVec<V> &it) const;
	bool operator!=(const RowOwner<V> &it) const;

	// reduce
	template<typename T>
	T reduce(const Monoid<T, V> &monoid) const;
	template<typename T, typename M>
	T reduce(const CompactRowVec<I, M> &mask, Monoid<T, V> &monoid) const;

	// set/get an element by the global index
	void setElement(I index, const V &val);
	V getElement(I index) const;
	// fill the vector with a specified value
	void fill(const V &val);
	// arr[i] <- i (for numeric value type only)
	void setToIndex();

	// free the allocated memory
	void freeMemory();

	const CommGrid* getCommGrid() const { return R_.getCommGrid(); }

private:
	const Range<I> &R_;
	V *ptr_;        // local C array
};

template<typename I, typename V>
class CompactColVec : public ColOwner<V> {
public:
	// constructor & deconstructor
	CompactColVec(const Range<I> &R);
	CompactColVec(const CompactColVec<I, V> &it);
	~CompactColVec();

	// get the C array pointer
	V* c_data() override;
	const V* c_data() const override;
	// size of owned data
	int c_size() const override;

	// swap
	void swap(CompactColVec<I, V> &it);

	// assignment
	CompactColVec<I, V>& operator=(const FullyDistColVec<I, V> &it);
	CompactColVec<I, V>& operator=(const ReadableColVec<V> &it);
	CompactColVec<I, V>& operator=(const ColOwner<V> &it);

	// comparison
	bool operator==(const FullyDistColVec<I, V> &it) const;
	bool operator==(const ReadableColVec<V> &it) const;
	bool operator==(const ColOwner<V> &it) const;

	bool operator!=(const FullyDistColVec<I, V> &it) const;
	bool operator!=(const ReadableColVec<V> &it) const;
	bool operator!=(const ColOwner<V> &it) const;

	// reduce
	template<typename T>
	T reduce(const Monoid<T, V> &monoid) const;
	template<typename T, typename M>
	T reduce(const CompactColVec<I, M> &mask, Monoid<T, V> &monoid) const;

	// set/get an element by the global index
	void setElement(I index, const V &val);
	V getElement(I index) const;
	// fill the vector with a specified value
	void fill(const V &val);
	// arr[i] <- i (for numeric value type only)
	void setToIndex();

	// free the allocated memory
	void freeMemory();

	const CommGrid* getCommGrid() const { return R_.getCommGrid(); }

private:
	const Range<I> &R_;
	V *ptr_;        // local C array
};

// -------------- CompactRowVec -------------- //

template<typename I, typename V>
CompactRowVec<I, V>::CompactRowVec(const Range<I> &R):R_(R) {
	ptr_  = new V[R_.rSize];
}

template<typename I, typename V>
CompactRowVec<I, V>::CompactRowVec(const CompactRowVec<I, V> &it):R_(it.R_) {
	ptr_ = new V[R_.rSize];
	std::copy(it.ptr_, it.ptr_ + R_.rSize, ptr_);
}

template<typename I, typename V>
CompactRowVec<I, V>::~CompactRowVec() {
	if (ptr_ != nullptr)
		delete[] ptr_;
}

template<typename I, typename V>
V* CompactRowVec<I, V>::r_data() {
	return ptr_;
}

template<typename I, typename V>
const V* CompactRowVec<I, V>::r_data() const {
	return ptr_;
}

template<typename I, typename V>
int CompactRowVec<I, V>::r_size() const {
	return R_.rSize;
}

template<typename I, typename V>
void CompactRowVec<I, V>::swap(CompactRowVec<I, V> &it) {
	std::swap(ptr_, it.ptr_);
}

template<typename I, typename V>
CompactRowVec<I, V>& CompactRowVec<I, V>::operator=(const FullyDistRowVec<I, V> &it) {
	std::copy(it.r_data(), it.r_data() + R_.rSize, ptr_);
	return *this;
}

template<typename I, typename V>
CompactRowVec<I, V>& CompactRowVec<I, V>::operator=(const ReadableRowVec<V> &it) {
	for (int i = 0; i < R_.rSize; i++)
		ptr_[i] = it[R_.rOffset + i];
	return *this;
}

template<typename I, typename V>
CompactRowVec<I, V>& CompactRowVec<I, V>::operator=(const RowOwner<V> &it) {
	std::copy(it.r_data(), it.r_data() + R_.rSize, ptr_);
	return *this;
}

template<typename I, typename V>
bool CompactRowVec<I, V>::operator==(const FullyDistRowVec<I, V> &it) const {
	int eq = !memcmp(ptr_, it.r_data(), sizeof(V) * R_.rSize);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool CompactRowVec<I, V>::operator==(const ReadableRowVec<V> &it) const {
	int eq = 1;
	for (int i = 0; eq && i < R_.rSize; i++)
		eq = (ptr_[i] == it[i + R_.rOffset]);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool CompactRowVec<I, V>::operator==(const RowOwner<V> &it) const {
	int eq = !memcmp(ptr_, it.r_data(), sizeof(V) * R_.rSize);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool CompactRowVec<I, V>::operator!=(const FullyDistRowVec<I, V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
bool CompactRowVec<I, V>::operator!=(const ReadableRowVec<V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
bool CompactRowVec<I, V>::operator!=(const RowOwner<V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
template<typename T>
T CompactRowVec<I, V>::reduce(const Monoid<T, V> &monoid) const {
	T val = monoid.id();
	for (int i = 0; i < R_.rSize; i++)
		monoid.add(val, ptr_[i]);
	return allReduce(val, monoid.mpiOp(), R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
template<typename T, typename M>
T CompactRowVec<I, V>::reduce(const CompactRowVec<I, M> &mask, Monoid<T, V> &monoid) const {
	T val = monoid.id();
	for (int i = 0; i < R_.rSize; i++)
		if (mask[i])
			monoid.add(val, ptr_[i]);
	return allReduce(val, monoid.mpiOp(), R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
void CompactRowVec<I, V>::setElement(I index, const V &val) {
	if (index >= R_.rOffset && index < R_.rOffset + R_.rSize)
		ptr_[index - R_.rOffset] = val;
}

template<typename I, typename V>
V CompactRowVec<I, V>::getElement(I index) const {
	V val;
	if (index >= R_.rOffset && index < R_.rOffset + R_.rSize)
		val = ptr_[index - R_.rOffset];
	int rank = this->commGrid_->getRank();
	MPI_Bcast(&val, 1, MPIType<V>(), rank, R_.getCommGrid()->getCommWorld());
	return val;
}

template<typename I, typename V>
void CompactRowVec<I, V>::fill(const V &val) {
	std::fill(ptr_, ptr_ + R_.rSize, val);
}

template<typename I, typename V>
void CompactRowVec<I, V>::setToIndex() {
	for (int i = 0; i < R_.rSize; i++)
		ptr_[i] = i + R_.rOffset;
}

template<typename I, typename V>
void CompactRowVec<I, V>::freeMemory() {
	if (ptr_ != nullptr) {
		delete[] ptr_;
		ptr_ = nullptr;
	}
}

// -------------- CompactColVec -------------- //

template<typename I, typename V>
CompactColVec<I, V>::CompactColVec(const Range<I> &R):R_(R) {
	ptr_  = new V[R_.cSize];
}

template<typename I, typename V>
CompactColVec<I, V>::CompactColVec(const CompactColVec<I, V> &it):R_(it.R_) {
	ptr_ = new V[R_.cSize];
	std::copy(it.ptr_, it.ptr_ + R_.cSize, ptr_);
}

template<typename I, typename V>
CompactColVec<I, V>::~CompactColVec() {
	if (ptr_ != nullptr)
		delete[] ptr_;
}

template<typename I, typename V>
V* CompactColVec<I, V>::c_data() {
	return ptr_;
}

template<typename I, typename V>
const V* CompactColVec<I, V>::c_data() const {
	return ptr_;
}

template<typename I, typename V>
int CompactColVec<I, V>::c_size() const {
	return R_.cSize;
}

template<typename I, typename V>
void CompactColVec<I, V>::swap(CompactColVec<I, V> &it) {
	std::swap(ptr_, it.ptr_);
}

template<typename I, typename V>
CompactColVec<I, V>& CompactColVec<I, V>::operator=(const FullyDistColVec<I, V> &it) {
	std::copy(it.c_data(), it.c_data() + R_.cSize, ptr_);
	return *this;
}

template<typename I, typename V>
CompactColVec<I, V>& CompactColVec<I, V>::operator=(const ReadableColVec<V> &it) {
	for (int i = 0; i < R_.cSize; i++)
		ptr_[i] = it[i + R_.cOffset];
	return *this;
}

template<typename I, typename V>
CompactColVec<I, V>& CompactColVec<I, V>::operator=(const ColOwner<V> &it) {
	std::copy(it.c_data(), it.c_data() + R_.cSize, ptr_);
	return *this;
}

template<typename I, typename V>
bool CompactColVec<I, V>::operator==(const FullyDistColVec<I, V> &it) const {
	int eq = !memcmp(ptr_, it.c_data(), sizeof(V) * R_.cSize);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool CompactColVec<I, V>::operator==(const ReadableColVec<V> &it) const {
	int eq = 1;
	for (int i = 0; eq && i < R_.cSize; i++)
		eq = (ptr_[i] == it[i + R_.cOffset]);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool CompactColVec<I, V>::operator==(const ColOwner<V> &it) const {
	int eq = !memcmp(ptr_, it.c_data(), sizeof(V) * R_.cSize);
	return allReduce(eq, MPI_LAND, R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
bool CompactColVec<I, V>::operator!=(const FullyDistColVec<I, V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
bool CompactColVec<I, V>::operator!=(const ReadableColVec<V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
bool CompactColVec<I, V>::operator!=(const ColOwner<V> &it) const {
	return !(*this == it);
}

template<typename I, typename V>
template<typename T>
T CompactColVec<I, V>::reduce(const Monoid<T, V> &monoid) const {
	T val = monoid.id();
	for (int i = 0; i < R_.cSize; i++)
		monoid.add(val, ptr_[i]);
	return allReduce(val, monoid.mpiOp(), R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
template<typename T, typename M>
T CompactColVec<I, V>::reduce(const CompactColVec<I, M> &mask, Monoid<T, V> &monoid) const {
	T val = monoid.id();
	for (int i = 0; i < R_.cSize; i++)
		if (mask[i])
			monoid.add(val, ptr_[i]);
	return allReduce(val, monoid.mpiOp(), R_.getCommGrid()->getCommWorld());
}

template<typename I, typename V>
void CompactColVec<I, V>::setElement(I index, const V &val) {
	if (index >= R_.cOffset && index < R_.cOffset + R_.cSize)
		ptr_[index - R_.cOffset] = val;
}

template<typename I, typename V>
V CompactColVec<I, V>::getElement(I index) const {
	V val;
	if (index >= R_.cOffset && index < R_.cOffset + R_.cSize)
		val = ptr_[index - R_.cOffset];
	int rank = this->commGrid_->getRank();
	MPI_Bcast(&val, 1, MPIType<V>(), rank, R_.getCommGrid()->getCommWorld());
	return val;
}

template<typename I, typename V>
void CompactColVec<I, V>::fill(const V &val) {
	std::fill(ptr_, ptr_ + R_.cSize, val);
}

template<typename I, typename V>
void CompactColVec<I, V>::setToIndex() {
	for (int i = 0; i < R_.cSize; i++)
		ptr_[i] = i + R_.cOffset;
}

template<typename I, typename V>
void CompactColVec<I, V>::freeMemory() {
	if (ptr_ != nullptr) {
		delete[] ptr_;
		ptr_ = nullptr;
	}
}

// -------------- Auxiliary functions -------------- //

template<typename I, typename V>
void copyRowToCol(const RowOwner<V> &row, CompactColVec<I, V> &col) {
	const CommGrid *commGrid = col.getCommGrid();
	const V *rPtr = row.r_data(); // read-only
	V *cPtr = col.c_data();       // writable
	MPI_Status status;
	if (commGrid->getRowGroup() != commGrid->getColGroup()) {
		int partner = commGrid->getPartner();
		MPI_Sendrecv(
			rPtr, row.r_size(), MPIType<V>(), partner, 0,
			cPtr, col.c_size(), MPIType<V>(), partner, 0,
			commGrid->getCommWorld(), &status);
	} else
		std::copy(rPtr, rPtr + row.r_size(), cPtr);
}

template<typename I, typename V>
void copyColToRow(const ColOwner<V> &col, CompactRowVec<I, V> &row) {
	const CommGrid *commGrid = row.getCommGrid();
	const V *cPtr = col.c_data(); // read-only
	V *rPtr = row.r_data();       // writable
	MPI_Status status;
	if (commGrid->getRowGroup() != commGrid->getColGroup()) {
		int partner = commGrid->getPartner();
		MPI_Sendrecv(
			cPtr, col.c_size(), MPIType<V>(), partner, 0,
			rPtr, row.r_size(), MPIType<V>(), partner, 0,
			commGrid->getCommWorld(), &status);
	} else
		std::copy(cPtr, cPtr + col.c_size(), rPtr);
}

#endif /* COMPACT_VEC_H_ */