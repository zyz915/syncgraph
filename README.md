A Lightweight Distributed Graph Processing System
===============


## Introduction ##

A distributed graph processing system trying to implement the full [GraphBLAS API Specification](http://people.eecs.berkeley.edu/~aydin/GraphBLAS_API_C_v13.pdf).

## Installation (for Ubuntu) ##

The installation requires the following softwares:

1. a C++ compiler (C++11 compatible)
2. cmake
3. mpich (OpenMPI should work as well)

### 1. Preparations ###

First, we need to install some necessary tools:

```
$ sudo apt-get update
$ sudo apt-get install cmake g++ vim wget git
```

### 2. Libraries ###

#### 2.1 MPICH

Then, download the MPI source code, compile the library and install it on your machine.
We highly recommend you to use the MPICH release, which can be download from [mpich.org](https://www.mpich.org/downloads/).
You can download the version 3.2 by the following command:

```
$ wget http://www.mpich.org/static/downloads/3.2/mpich-3.2.tar.gz
```

Next, unpack the package:

```
$ tar -xzf mpich-3.2.tar.gz
$ cd mpich-3.2
```

Compile an install the MPICH library:

```
$ ./configure --prefix=/usr/local/mpich --disable-fortran
$ make
$ sudo make install
```

Then the MPICH is installed in `/usr/local/mpich`.
To see whether the installation succeeds, type:

```
$ ls /usr/local/mpich
```

### 3. Environment Variables ###

Edit the file `~/.bashrc` and add the following lines in the end:

```
export MPICH_HOME="/usr/lib/mpich"
```

The `CMakeList.txt` file will check this environment variable to use MPI.

### 4. Compilation ###

Finally, we are going to download and compile the code, which can be done by the following commands:

```
$ git clone https://bitbucket.org/zyz915/syncgraph
$ cd syncgraph
$ mkdir build
$ cd build
$ cmake ..
$ make
$ ls
```

Then, you can find the executables in the current folder.
We currently have implemented the following algorithms:

* PageRank
* Single-Source Shortest Path (SSSP)
* Betweenness Centrality (BC)
* Strongly Connected Components (SCC)
* Boruvka's Minimum Spanning Forest (MSF)
* (Weakly) Connected Componects (WCC/CC)
	* FastSV Algorithm
    * Label Propagation (LP)
    * Boruvka's MSF Algorithm (simplified)
