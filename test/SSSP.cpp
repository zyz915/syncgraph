#include <algorithm>
#include <vector>

#include "api.h"
#include "SSSP.h"

using namespace std;

void checkSolution(GraphAPI<int> &GrA, const Graph<int, int> &graph,
		const FullyDistVec<int, int> &dist)
{
	int n = GrA.numVertices(graph);
	int *f = new int[n];
	fill(f, f + n, INT_MAX);
	f[0] = 0;
	vector<EdgeUnit<int, int> > tuples;
	graph2Tuple(GrA, graph, tuples);
	while (true) {
		bool modified = false;
		for (int i = 0; i < tuples.size(); i++) {
			int u = tuples[i].src;
			int v = tuples[i].dst;
			int w = tuples[i].weight;
			if (f[u] != INT_MAX && f[u] + w < f[v]) {
				modified = true;
				f[v] = f[u] + w;
			}
		}
		if (!modified) break;
	}
	const int *d = dist.col().getPtr();
	if (memcmp(f, d, sizeof(int) *n)) {
		printf("wrong answer!\n");
	} else {
		printf("correct!\n");
	}
}

int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	CommGrid *commGrid = new CommGrid(MPI_COMM_WORLD);
	Param param = parseParam(argc, argv);
	Graph<int, int> graph;
	loadWeightedGraph(commGrid, param, graph);
	GraphAPI<int> GrA(commGrid, graph.n);

	double t1, t2;
	t1 = MPI_Wtime();
	FullyDistVec<int, int> result(GrA);
	SSSP(GrA, graph, result, 0);
	t2 = MPI_Wtime();

	if (GrA.getCommGrid()->getRank() == 0) {
		printf("elapsed time %f\n", t2 - t1);
	}
	if (GrA.getCommGrid()->getSize() == 1)
		checkSolution(GrA, graph, result);
	return 0;
}