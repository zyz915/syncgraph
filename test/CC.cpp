#include "api.h"
#include "CC.h"
#include "FastSV.h"
#include "FastSV_v2.h"
#include "LP.h"

int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	CommGrid *commGrid = new CommGrid(MPI_COMM_WORLD);
	Param param = parseParam(argc, argv);
	param.symm = 1;
	Graph<int, bool> graph;
	CSRFormat<int, bool> csr;
	loadGraph(commGrid, param, graph);
	loadGraph(commGrid, param, csr);
	GraphAPI<int> GrA(commGrid, graph.n);
	// compute
	FullyDistVec<int, int> f(GrA); // result
	IndexVec<int> ind(GrA); // ind[i] = i
	double t1, t2;
	int cc1, cc2, cc3, cc4;      // answer
	double rt1, rt2, rt3, rt4;   // runtime

	t1 = MPI_Wtime();
	CC_boruvka (GrA, graph, f);
	t2 = MPI_Wtime();
	cc1 = GrA.reduce(zip(f.row(), ind.row(), EQ<int>()), PLUS<int>());
	rt1 = t2 - t1;

	t1 = MPI_Wtime();
	CC_FastSV (GrA, graph, f);
	t2 = MPI_Wtime();
	cc2 = GrA.reduce(zip(f.row(), ind.row(), EQ<int>()), PLUS<int>());
	rt2 = t2 - t1;

	t1 = MPI_Wtime();
	CC_FastSV_v2 (GrA, csr, f);
	t2 = MPI_Wtime();
	cc3 = GrA.reduce(zip(f.row(), ind.row(), EQ<int>()), PLUS<int>());
	rt3 = t2 - t1;

	t1 = MPI_Wtime();
	CC_Propagation (GrA, graph, f);
	t2 = MPI_Wtime();
	cc4 = GrA.reduce(zip(f.row(), ind.row(), EQ<int>()), PLUS<int>());
	rt4 = t2 - t1;

	if (commGrid->getRank() == 0) {
		printf(" %-12s %-6s %-8s\n", "Algorithm", "nCCs", "Runtime");
		printf(" %-12s %-6d %-8.4f\n", "Boruvka",   cc1, rt1);
		printf(" %-12s %-6d %-8.4f\n", "FastSV",    cc2, rt2);
		printf(" %-12s %-6d %-8.4f\n", "FastSV_v2", cc3, rt3);
		printf(" %-12s %-6d %-8.4f\n", "Propagation", cc4, rt4);
	}
	return 0;
}
