#include <vector>

#include "api.h"
#include "MSF.h"

using namespace std;

long long Kruskal (const vector<EdgeUnit<int, int> > &tuples, int *f, int n)
{
	for (int i = 0; i < n; i++)
		f[i] = i;
	long long answer = 0;
	for (int i = 0; i < tuples.size(); i++) {
		int x = tuples[i].src;
		int y = tuples[i].dst;
		bool comb = false;
		while (f[x] != f[y]) {
			if (f[x] > f[y]) {
				if (f[x] == x) {
					comb = true;
					f[x] = f[y];
					break;
				}
				int t = f[x];
				f[x] = f[y];
				x = t;
			} else {
				if (f[y] == y) {
					comb = true;
					f[y] = f[x];
					break;
				}
				int t = f[y];
				f[y] = f[x];
				y = t;
			}
		}
		if (comb)
			answer += tuples[i].weight;
	}
	return answer;
}

void checkSolution (GraphAPI<int> &GrA, const Graph<int, int> &graph, const Graph<int, int> &result)
{
	vector<EdgeUnit<int, int> > tuples;
	// check the spanning forest
	int n = GrA.numVertices(graph);
	int *f = new int[n], *g = new int[n];
	graph2Tuple(GrA, graph, tuples);
	long long ans = Kruskal(tuples, f, n);
	graph2Tuple(GrA, result, tuples);
	long long sol = Kruskal(tuples, g, n);
	int same = !memcmp(f, g, sizeof(int) * n);
	if (ans != sol || !same) {
		printf("wrong answer!\n");
		printf("expected : %lld\n", ans);
		printf("actual   : %lld\n", sol);
		exit(0);
	} else {
		printf("correct (sum = %lld)\n", ans);
	}
	free(f); free(g);
}


int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	CommGrid *commGrid = new CommGrid(MPI_COMM_WORLD);
	Param param = parseParam(argc, argv);
	param.symm = 1;
	Graph<int, int> graph, result;
	loadWeightedGraph(commGrid, param, graph);
	GraphAPI<int> GrA(commGrid, graph.n);

	double t1, t2;
	t1 = MPI_Wtime();
	MSF(GrA, graph, result);
	t2 = MPI_Wtime();

	if (GrA.getCommGrid()->getRank() == 0) {
		printf("elapsed time %f\n", t2 - t1);
	}
	if (GrA.getCommGrid()->getSize() == 1)
		checkSolution(GrA, graph, result);
	return 0;
}