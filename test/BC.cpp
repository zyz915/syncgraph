#include "api.h"
#include "BC.h"

int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	CommGrid *commGrid = new CommGrid(MPI_COMM_WORLD);
	int rank = commGrid->getRank();
	// load file
	Param param = parseParam(argc, argv);
	Graph<int, bool> graph;
	loadGraph(commGrid, param, graph);
	int n = param.n;
	GraphAPI<int> GrA(commGrid, n);

	// computation
	double t1 = MPI_Wtime();
	FullyDistRowVec<int, double> cent(GrA), brandes(GrA);
	brandes.fill(0.0);
	for (int i = 0; i < n; i++) {
		BC (GrA, graph, cent, i);
		brandes = zip(brandes, cent, PLUS<double>());
	}
	double t2 = MPI_Wtime();
	brandes.debugPrint(GrA);
	if (commGrid->getRank() == 0)
		printf("elapsed time: %f\n", t2 - t1);
	return 0;
}
