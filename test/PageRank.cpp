#include "api.h"
#include "PageRank.h"

int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	CommGrid *commGrid = new CommGrid(MPI_COMM_WORLD);
	Param param = parseParam(argc, argv);
	Graph<int, bool> graph;
	loadGraph(commGrid, param, graph);
	GraphAPI<int> GrA(commGrid, param.n);

	double t1, t2;
	t1 = MPI_Wtime();
	FullyDistVec<int, double> result(GrA);
	PageRank(GrA, graph, result, 30);
	t2 = MPI_Wtime();

	if (GrA.getCommGrid()->getRank() == 0) {
		printf("PageRank %f\n", t2 - t1);
	}
	return 0;
}