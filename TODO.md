TODO list
------------

General issues

- lots of bugs after refactoring ..
- higher order functions:
    - unique pointer??
- implement the fully distributed sparse vectors
- implement the SpMSpV (for FastSV)
- test drive
- algorithms as headers
- add mask in all linear algebra operation
    - how to deal with row/column?
- redesign of extract
    - dst[index[i]] = src[i] for i in [0..n] is implemented by
    - Extract(index).copy(src, dst, Monoid??)

Algorithms:

- reimplement MSF, SCC, BC, SSSP, LP using the new interface

GraphBLAS API

- design a more efficient strategy for implementing the GraphBLAS standard
    - currently, everything is FullyDistVec which incurs quite a lot of overhead
    - try dynamically selecting the Row/Column vector
- make use of C++'s type system in GraphBLAS's API design
