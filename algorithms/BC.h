// Brandes' algorithm for computing betweeness centrality

#include "api.h"

// output: centrality
template<typename I, typename V>
void BC (GraphAPI<I> &GrA, const Graph<I, V> &graph,
		FullyDistRowVec<I, double> &centrality, I source = 0)
{
	FullyDistVec<I, int64_t> frontier(GrA);
	FullyDistRowVec<I, int64_t> paths(GrA);
	FullyDistRowVec<I, int> visited(GrA);
	ConstantVec<int64_t> one(1), zero(0);
	frontier.fill(0);
	frontier.setElement(source, 1);
	visited.fill(0);
	visited.setElement(source, 1);
	paths = zero.row();
	int depth = 1;

	while (true) {
		depth = depth + 1;
		frontier.col() = zero.col();
		GrA.push(graph, frontier.row(), visited, PLUS<int64_t>(), frontier.col());
		paths = zip(paths, frontier.row(), PLUS<int64_t>());
		copyColToRow(frontier.col(), frontier.row());
		ConstantVec<int> depVec(depth);
		frontier.row().set(visited, zero.row());
		I sum = GrA.reduce(zip(frontier.row(), zero.row(), NE<I, int64_t>()), PLUS<I>());
		if (sum == 0) break;
		visited.set(frontier.row(), depVec.row());
	}
	frontier.freeMemory();

	FullyDistRowVec<I, double> tmp(GrA);
	FullyDistRowVec<I, double> ext(GrA); // extract the #paths
	FullyDistColVec<I, double> prod(GrA);
	ConstantVec<double> one_f(1), zero_f(0);
	Graph<I, V> G;
	transpose(GrA, graph, G);

	centrality = zero_f.row();
	// ext[i] = (visited[i] == depth - 1 ? (double) paths[i] : 0)
	ConstantRowVec<int> depthMinus1(depth - 1);
	ext = zero_f.row();
	ext.set(
			zip(visited, depthMinus1, EQ<int>()),  // mask
			map(paths, CAST<double, int64_t>()));  // RHS
	for (int d = depth - 2; d > 1; d--) {
		// tmp = 1 + centrality
		tmp = zip(centrality, one_f.row(), PLUS<double>());
		// tmp = tmp ./ ext
		tmp.set(ext, zip(tmp, ext, DIV<double>()));
		// temp2 = A_matrix * tmp
		prod = zero_f.col();
		GrA.push(G, tmp, ext, PLUS<double>(), prod);
		copyColToRow(prod, tmp);
		// ext[i] = (visited[i] == d ? (double) paths[i] : 0)
		ConstantRowVec<int> curDepth(d);
		ext = zero_f.row();
		ext.set(
				zip(visited, curDepth, EQ<int>()),     // mask
				map(paths, CAST<double, int64_t>()));  // RHS
		// Compute the entire update for the centrality scores
		// Update centrality scores
		// centrality += tmp
		centrality = zip(centrality, zip(tmp, ext, TIMES<double>()), PLUS<double>());
	}
	G.freeMemory();
}
