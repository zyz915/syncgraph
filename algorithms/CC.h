#pragma once
// Boruvka's MSF algorithm, simplified for finding connected components

#include "api.h"

template<typename I = int, typename V = bool>
void CC_boruvka (GraphAPI<I> &GrA, const Graph<I, V> &graph, FullyDistVec<I, I> &result)
{
	Graph<I, V> G;
	GrA.duplicate(graph, G);
	int n = GrA.numVertices(graph);
	FullyDistVec<I, I> m(GrA), &f = result;
	FullyDistColVec<I, I> g(GrA);
	ConstantVec<I> inf(n); // inf[i] = n
	IndexVec<I> ind(GrA);  // ind[i] = i
	f = ind;

	while (true)
	{
		// minimum neighbor's parent
		m = inf; // m(:) = n
		GrA.push(G, f.row(), MIN<I>(), m.col());
		// m[f[i]] <- min(m[f[i]], m[i])
		GrA.reduceAssign(m.col(), f.col(), MIN<I>(), m.row());
		// if (f[i] != i && m[i] != n) f[i] = m[i]
		f.row().set(zip(
				zip(f.row(), ind.row(), EQ<I>()),
				zip(m.row(), inf.row(), NE<I>()),
				LAND<I>()), m.row());
		copyRowToCol(f.row(), f.col());
		// identify supervertex (f[u] == v && f[v] == u)
		// TODO: can be improved
		GrA.reduceExtract(f.row(), f.col(), MIN<I>(), m.col());
		f.col().set(
			zip(m.col(), ind.col(), EQ<I>()),  // mask
			zip(f.col(), m.col(), MIN<I>()));  // RHS
		copyColToRow(f.col(), f.row());
		while (true)
		{
			g = inf.col(); // g(:) = n
			GrA.reduceExtract(f.row(), f.col(), MIN<I>(), g);
			if (g == f.col()) break;
			f.col() = g;
			copyColToRow(f.col(), f.row());
		}
		// edge removal:
		// select the edges between different trees
		I *f_row = f.row().getPtr(), *f_col = f.col().getPtr();
		SelectOp<V> selOp = [f_row, f_col](int u, int v, V w) {
			return (f_row[u] != f_col[v]);
		};
		GrA.select(G, selOp);
		int64_t sum = GrA.numEdges(G);
		if (sum == 0) break;
	}
	G.freeMemory();
}
