#pragma once
// Boruvka's Minimum Spanning Forest (MSF) algorithm

#include "api.h"

class Combine : public BinaryOp<uint64_t, int, int> {
public:
	uint64_t mult(const int &a, const int &x) const override {
		return (((uint64_t) a) << 32) + x;
	}
};

class GetFirst : public UnaryOp<int, uint64_t> {
public:
	int apply(const uint64_t &encoded) const override {
		return encoded >> 32;
	}
};

class GetSecond : public UnaryOp<int, uint64_t> {
public:
	int apply(const uint64_t &encoded) const override {
		return (int) encoded;
	}
};

void MSF (GraphAPI<int> &GrA, const Graph<int, int> &graph, Graph<int, int> &result)
{
	Graph<int, int> G;
	GrA.duplicate(graph, G);
	FullyDistVec<int, int> f(GrA);  // parent
	FullyDistRowVec<int, int> picked(GrA);
	FullyDistColVec<int, int> parent(GrA), g(GrA), index(GrA), mask(GrA);
	FullyDistRowVec<int, uint64_t> r_edge(GrA);  // edge chosen by the root
	FullyDistColVec<int, uint64_t> edge(GrA), p_edge(GrA);
	// semiring (TODO)
	MIN<uint64_t> MIN_UINT64;
	Combine combine;
	auto CombineMin = Semiring<uint64_t, int, int>(MIN_UINT64, combine);
	GetFirst first;
	GetSecond second;
	int n = GrA.numVertices(G);
	// space-efficient read-only vectors
	uint64_t max_ll = (((uint64_t) INT_MAX) << 32) | ((uint64_t) INT_MAX);
	ConstantVec<int> inf32(INT_MAX);
	ConstantVec<uint64_t> inf64(max_ll);
	IndexVec<int> ind(GrA);  // ind[i] == i
	f = ind;
	// solution
	vector<EdgeUnit<int, int> > tuples;
	int64_t sum = GrA.numEdges(G);

	while (sum > 0)
	{
		// minimum edge selection
		// edge[v] is v's minimum edge (w, f[u])
		edge = inf64.col();
		GrA.push(G, f.row(), CombineMin, edge);
		// r_edge[f[i]] <- edge[i]
		r_edge = zip(inf32.row(), ind.row(), Combine());
		GrA.reduceAssign(edge, f.col(), MIN<uint64_t>(), r_edge);
		// parent-child relation at this moment
		parent = f.col();
		// if f[i] == i then f[i] = snd(r_edge[i])
		f.row().set(
				zip(f.row(), ind.row(), EQ<int>()), // mask
				map(r_edge, GetSecond()));          // RHS
		copyRowToCol(f.row(), f.col());
		// identify supervertex (f[u] == v && f[v] == u)
		// at least one processor will see both f[u] && f[v]
		// if (f[f[u]] == u) f[u] = min(f[u], u)
		g.fill(n);
		GrA.reduceExtract(f.row(), f.col(), MIN<int>(), g);
		f.col().set(
				zip(g, ind.col(), EQ<int>()),  // mask
				zip(f.col(), g, MIN<int>()));  // RHS
		f.syncColToRow(MPI_MIN);
		// print solution
		// 1. new roots (f[i] == i) revise their entries in r_edge
		r_edge.set(zip(f.row(), ind.row(), EQ<int>()), inf64.row());
		// 2. every vertex tries to know whether one of its edges is selected
		p_edge = inf64.col();
		GrA.reduceExtract(r_edge, parent, MIN<uint64_t>(), p_edge);
		mask = zip(edge, p_edge, EQ<int, uint64_t>());
		// 3. each root picks a vertex from its children to generate the solution
		index.fill(n);
		index.set(mask, ind.col()); // if (mask[i]) index[i] = i
		picked.fill(n);
		GrA.reduceAssign(index, parent, MIN<int>(), picked);  // c -> c -> r
		GrA.reduceExtract(picked, parent, MIN<int>(), index); // r -> c -> c
		mask = zip(ind.col(), index, EQ<int>());
		// 4. generate the select function (set the global pointers)
		edge.set(map(mask, NOT<int>()), inf64.col());
		g = map(edge, GetFirst());
		index = map(edge, GetSecond());
		copyColToRow(parent, picked); // reuse the picked vector
		int *c_weight  = g.getPtr();
		int *c_partner = index.getPtr();
		int *r_parent = picked.getPtr();
		SelectOp<int> match = [c_weight, c_partner, r_parent] (int u, int v, int w) {
			return (c_weight[v] == w) && (r_parent[u] == c_partner[v]);
		};
		edge.fill(max_ll);
		GrA.push(G, ind.row(), CombineMin, edge, match);
		// store the selected edges as tuples
		for (int i = 0; i < GrA.cLength; i++)
			if (mask[i]) {
				int v = i + GrA.cStart;
				int u = second.apply(edge[i]);
				int w = first.apply(edge[i]);
				if (u >= GrA.rStart && u < GrA.rEnd)
					tuples.push_back(EdgeUnit<int, int>(u, v, w));
			}
		// path halving until all vertices are pointing to a root
		while (true)
		{
			g.fill(INT_MAX);
			GrA.reduceExtract(f.row(), f.col(), MIN<int>(), g);
			if (g == f.col()) break;
			f.col() = g;
			copyColToRow(f.col(), f.row());
		}
		// edge removal
		int *f_row = f.row().getPtr(), *f_col = f.col().getPtr();
		SelectOp<int> eRemoval = [f_row, f_col](int u, int v, int w){
			return f_row[u] != f_col[v];
		};
		GrA.select(G, eRemoval);
		sum = GrA.numEdges(G);
	}
	G.freeMemory();
	tuple2Graph(GrA, tuples, result);
}