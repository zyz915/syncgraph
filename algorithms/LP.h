#pragma once
// finding connected components using label propagation (LP)

#include "api.h"

template<typename I, typename V>
void CC_Propagation (GraphAPI<I> GrA, const Graph<I, V> &graph, FullyDistVec<I, I> &result)
{
	FullyDistVec<I, I> &label = result;
	IndexVec<I> ind(GrA);
	label = ind;  // label[i] = i;
	GrA.propagate(graph, MIN<I>(), label);
}
