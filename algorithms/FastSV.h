#pragma once
// implement the FastSV algorithm for finding connected components

#include "api.h"

template<typename I, typename V>
void CC_FastSV (GraphAPI<I> GrA, const Graph<I, V> &graph, FullyDistVec<I, I> &result)
{
	FullyDistVec<int, int> g(GrA), m(GrA), &f = result;
	CompactColVec<int, int> d(GrA);
	IndexVec<int> ind(GrA);
	f = ind;
	g = f; // grandparent
	m = f; // minimum neighbor's grandparent
	d = f.col(); // duplication of g

double t1, t2;
double t_mxv, t_assign, t_extract, t_others;
int iters = 0;
	while (true)
	{
t1 = MPI_Wtime();
		// minimum neighbor's grandparent
		GrA.push(graph, g.row(), MIN<int>(), m.col());
t2 = MPI_Wtime();
t_mxv = t2 - t1;
		copyColToRow(m.col(), m.row());
t1 = MPI_Wtime();
		// hooking
		GrA.reduceAssign(m.row(), f.row(), MIN<int>(), f.col());
		f.col() = zip(zip(m.col(), g.col(), MIN<int>()), g.col(), MIN<int>());
t2 = MPI_Wtime();
t_assign = t2 - t1;
		copyColToRow(f.col(), f.row());
		// calculate grandparent
t1 = MPI_Wtime();
		GrA.reduceExtract(f.row(), f.col(), MIN<int>(), g.col());
t2 = MPI_Wtime();
t_extract = t2 - t1;
		copyColToRow(g.col(), g.row());
		if (d == g.col()) break;
		d = g.col();
printf("iteration %d: t_mxv %f t_assign %f t_extract %f\n",
++iters, t_mxv, t_assign, t_extract);
	}
}
