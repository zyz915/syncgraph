// Strongly Connected Component (SCC)

#include <mpi.h>

#include "api.h"
#include "commgrid.h"
#include "fully_dist_vec.h"
#include "global.h"

int main(int argc, char **argv)
{
	MPI_Init(nullptr, nullptr);
	CommGrid *commGrid = new CommGrid(MPI_COMM_WORLD);
	Param param = parseParam(argc, argv);
	CSCFormat<bool> fw, bw; // original graph and the transposed graph
	loadGraph(commGrid, param, fw);
	transpose(commGrid, fw, bw);
	int n = fw.getDim();

	// compute
	double t1 = MPI_Wtime();
	GraphAPI GrA(commGrid, n);

	FullyDistColVec<int> ideg(GrA), odeg(GrA);
	//FullyDistRowVec<int> d(GrA), a(GrA);
	FullyDistVec<int> scc(GrA);
	FullyDistVec<int> f(GrA), g(GrA);
	scc.fill(-1);

	int *f_row = f.getRowPtr(), *f_col = f.getColPtr();
	int *g_row = g.getRowPtr(), *g_col = g.getColPtr();
	int *scc_row = scc.getRowPtr(), *scc_col = scc.getColPtr();

	// select the edges between non-SCCs
	SelectOp<bool> f1 = [scc_row, scc_col](int u, int v, bool w) {
		return scc_row[u] == -1 && scc_col[v] == -1;
	};
	// select the edges between the vertices with different FW/BW labels
	SelectOp<bool> f2 = [f_row, f_col, g_row, g_col](int u, int v, bool w) {
		return (f_col[v] != f_row[u] || g_col[v] != g_row[u]);
	};
	for (int v = 0; v < GrA.cLength; v++) {
		ideg[v] = fw.pos[v + 1] - fw.pos[v];
		odeg[v] = bw.pos[v + 1] - bw.pos[v];
	}
	odeg.sync(MPI_SUM);
	ideg.sync(MPI_SUM);

	while (true)
	{
		int curr = scc.count(-1);
		int iters = 0;
		while (curr > 0)
		{
			iters++;
			// trim-2 (step 1)
			f.fill(INT_MAX);
			g.fill(INT_MAX);
			for (int v = 0; v < GrA.cLength; v++)
				if (scc_col[v] == -1) {
					// p[u] = v iff. u -> v and out-deg[u] == 1
					if (odeg[v] == 1 && fw.pos[v] < fw.pos[v + 1])
						f_col[v] = fw.data[fw.pos[v]] + GrA.rStart;
					// q[u] = v iff. u <- v and in-deg[u] == 1
					if (ideg[v] == 1 && bw.pos[v] < bw.pos[v + 1])
						g_col[v] = bw.data[bw.pos[v]] + GrA.rStart;
				}
			f.syncColToRow(MPI_MIN);
			g.syncColToRow(MPI_MIN);

			for (int v = 0; v < GrA.cLength; v++) {
				// trim-2 (step 2)
				// p[u] == v and p[v] == u
				if (f_col[v] >= GrA.rStart && f_col[v] < GrA.rEnd) {
					int u = f_col[v] - GrA.rStart;
					if (f_row[u] == v + GrA.cStart)
						scc_col[v] = std::min(u + GrA.rStart, v + GrA.cStart);
				}
				// q[u] == v and q[v] == u
				if (g_col[v] >= GrA.rStart && g_col[v] < GrA.rEnd) {
					int u = g_col[v] - GrA.rStart;
					if (g_row[u] == v + GrA.cStart)
						scc_col[v] = std::min(u + GrA.rStart, v + GrA.cStart);
				}
				// trim-1
				if (scc_col[v] == -1 && (!ideg[v] || !odeg[v]))
					scc_col[v] = v + GrA.cStart;
			}
			scc.syncColToRow(MPI_MAX);

			// termination
			int prev = curr;
			curr = scc.count(-1);
			if (curr == prev) break;

			// edge removal
			GrA.select(fw, f1);
			GrA.select(bw, f1);

			for (int v = 0; v < GrA.cLength; v++) {
				ideg[v] = fw.pos[v + 1] - fw.pos[v];
				odeg[v] = bw.pos[v + 1] - bw.pos[v];
			}
			odeg.sync(MPI_SUM);
			ideg.sync(MPI_SUM);
		}

		// forward propagation
		f.setToIndex();
		LP_Propagate(commGrid, fw, f);
		// backward propagation
		g.setToIndex();
		g.zip<int>(f, [](int g, int f) { return g != f ? INT_MAX : g; });
		g.zip<int>(scc, [](int g, int s) { return s != -1 ? INT_MAX : g; });
		LP_Propagate(commGrid, bw, g);

		// identify SCCs
		for (int u = 0; u < GrA.rLength; u++)
			if (f_row[u] == g_row[u])
				scc_row[u] = f_row[u];
		scc.syncRowToCol(MPI_MAX);

		// edge removal
		GrA.select(fw, f2);
		GrA.select(bw, f2);

		int64_t rem = GrA.numEdges(fw);
		if (rem == 0) break;

		for (int v = 0; v < GrA.cLength; v++) {
			ideg[v] = fw.pos[v + 1] - fw.pos[v];
			odeg[v] = bw.pos[v + 1] - bw.pos[v];
		}
		odeg.sync(MPI_SUM);
		ideg.sync(MPI_SUM);
	}
	double t2 = MPI_Wtime();

	f.setToIndex();
	g.zip<int, int>(f, scc, std::equal_to<int>());
	int nscc = g.reduce<int>(std::plus<int>(), 0, MPI_SUM);

	if (commGrid->getRank() == 0) {
		printf("number of SCCs: %d\n", nscc);
		printf("elapsed time: %f\n", t2 - t1);
	}
	return 0;
}