#pragma once
// implement the FastSV algorithm for finding connected components

#include "api.h"

class Min : public ACCUM<int, int> {
public:
	int operator()(const int &x, const int &y) const override {
		return std::min(x, y);
	}
};

template<typename I = int, typename V = bool>
void CC_FastSV_v2 (GraphAPI<I> GrA, const CSRFormat<I, V> &graph, FullyDistVec<I, I> &result)
{
double T0 = MPI_Wtime();
	FullyDistVec<I, I> &f = result;
	FullyDistColVec<I, I> m(GrA);
	FullyDistRowVec<I, I> g(GrA);
	CompactRowVec<I, I> d(GrA), r(GrA);
	IndexVec<I> ind(GrA);
	f = ind;
	g = f.row(); // grandparent
	m = f.col(); // minimum neighbor's grandparent
	d = g;       // duplication of g
	I *f_row = f.getRowPtr(), *f_col = f.getColPtr();

	MIN<int> MIN_INT32;
double t_mxv = 0.0;
	while (true)
	{
double T1 = MPI_Wtime();
		// minimum neighbor's grandparent
		Min op;
		GrA.push(graph, g, op , m);
/*
	int *f_row = f.row().getPtr();
	for (int u = 0; u < GrA.rLength; u++)
		for (int64_t i = graph.pos[u]; i < graph.pos[u + 1]; i++) {
			int v = graph.colidx[i];
			m[v] = min(m[v], f_row[u]);
			//accum.add(col[v], row[u]);
		}
	m.sync(MPI_MIN);
*/
double T2 = MPI_Wtime();
t_mxv += T2 - T1;
		copyColToRow(m, r); // r := m
		// hooking (assign)
		for (int v = 0; v < GrA.cLength; v++)
			if (f_col[v] >= GrA.rStart && f_col[v] < GrA.rEnd) {
				int u = f_col[v] - GrA.rStart;
				f_row[u] = std::min(f_row[u], m[v]);
			}
		// partial zip
		int offset = GrA.rOffset;
		int *r_ptr = r.r_data();
		for (int i = 0; i < r.r_size(); i++)
			r_ptr[i] = std::min(r_ptr[i], g[i + offset]);
		for (int i = 0; i < r.r_size(); i++)
			if (r_ptr[i] < f_row[i + offset])
				f_row[i + offset] = r_ptr[i];
		f.syncRowToCol(MPI_MIN);
		// calculate grandparent (extract)
		#pragma omp parallel for
		for (int u = 0; u < GrA.rLength; u++)
			if (f_row[u] >= GrA.cStart && f_row[u] < GrA.cEnd) {
				int v = f_row[u] - GrA.cStart;
				g[u] = std::min(g[u], f_col[v]);
			}
		g.sync(MPI_MIN);
		if (d == g) break;
		d = g;
	}
double T3 = MPI_Wtime();
if (GrA.getCommGrid()->getRank() == 0)
printf("FastSV_v2 %f GrB_mxv %f\n", T3 - T0, t_mxv);
}
