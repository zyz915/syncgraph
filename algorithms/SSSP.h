// single-source shortest path (SSSP)

#include <numeric>
#include "api.h"

template<typename I, typename V>
void SSSP (GraphAPI<I> &GrA, const Graph<I, V> &graph,
		FullyDistVec<I, V> &dist, I source = 0)
{
	FullyDistRowVec<I, char> mask(GrA);
	dist.fill(std::numeric_limits<V>::max());
	dist.setElement(source, 0);
	mask.fill(static_cast<V>(0));
	mask.setElement(source, 1);
	GrA.propagate(graph, mask, Semiring<V, V>(MIN<V>(), PLUS<V>()), dist);
}