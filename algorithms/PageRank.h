// PageRank

#include "api.h"

class NewPR : public UnaryOp<double, double> {
public:
	NewPR(double sink_per_vertex):
			spv(sink_per_vertex) {}

	double apply(const double &pr) const override {
		return (pr + spv) * 0.85 + 0.15;
	}
private:
	double spv;
};

template<typename I, typename V>
void PageRank(GraphAPI<I> &GrA, const Graph<I, V> &graph,
		FullyDistVec<I, double> &pr, int rounds = 30)
{
	I n = GrA.numVertices(graph);
	// calculate the out-degree
	FullyDistRowVec<I, I> deg(GrA);
	GrA.degree(graph, deg);
	ConstantVec<double> one(1.0), zero(0.0);
	// inversed degree (inv[i] = 1 / deg[i]) if (deg[i] != 0)
	FullyDistRowVec<I, double> inv(GrA);
	inv = zero.row();
	inv.set(deg, zip(one.row(), map(deg, CAST<double, int>()), DIV<double>()));
	pr = one;

	for (int i = 0; i < GrA.rLength; i++) {
		inv[i] = (deg[i] == 0 ? 0.0 : 1.0 / deg[i]);
		deg[i] = !deg[i]; // mask
	}
	for (int iter = 0; iter < rounds; iter++)
	{
		// calculate the sink value: sink = sum [ pr[u] for u if deg[u] == 0 ]
		double sink = pr.row().reduce(deg, PLUS<double>());
		// calculate the pagerank without the sink node
		pr.row() = zip(pr.row(), inv, TIMES<double>());
		pr.col() = zero.col();
		GrA.push(graph, pr.row(), PLUS<double>(), pr.col());
		copyColToRow(pr.col(), pr.row());
		// combine these two results
		pr.row() = map(pr.row(), NewPR(sink / n));
	}
	ConstantVec<double> norm(1.0 / n);
	pr = zip(pr, norm, TIMES<double>());
}